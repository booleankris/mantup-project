<?php 
	session_start();
 ?>
<!DOCTYPE html>
<?php 
	include 'functions.php';
	$image_url = "assets/images/";
	$css_url = "assets/css/";
	$client = $_SESSION["id_client"][0]["id"];
	$idp = $_GET['idp'];
	if (isset($GET['idp'])) {
		$idp = $_GET['idp'];
	}
	$jumlahOnProgress = query("SELECT COUNT(id) FROM project");
	$cek_ongoing_project = mysqli_query($conn,"SELECT * FROM project WHERE id_client ='$client'");
	while($f = mysqli_fetch_array($cek_ongoing_project)){
		$id_client = $f['id_client'];
		$id_project = $f['id'];
	}
	$cek_project = mysqli_query($conn,"SELECT * FROM accepted_project WHERE id ='$idp' AND tahap_project != 'Selesai'");
	$cp = mysqli_fetch_array($cek_project);
	$idpro = $cp['id_project'];
	$det_project = mysqli_query($conn,"SELECT * FROM project WHERE id ='$idpro'");
	$det = mysqli_fetch_array($det_project);
	$hitung_project = mysqli_query($conn,"SELECT COUNT(id) AS jp FROM accepted_project WHERE id ='$idp' AND tahap_project != 'Selesai'");
	$fp = mysqli_fetch_array($hitung_project);
	$pro = $fp['jp'];
	$progress = $cp['progress'];
 ?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Ongoing Project</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #f1f1f1;
		}
	</style>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
					include 'includes/header.php';

				 ?>
			</div>
		</div>
	</nav>
	<style type="text/css">
		body{
			background-color: #2586d4;
		}
		::-webkit-input-placeholder { /* Chrome/Opera/Safari */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		::-moz-placeholder { /* Firefox 19+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-ms-input-placeholder { /* IE 10+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-moz-placeholder { /* Firefox 18- */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
	</style>
	<div class="project-content">
		<?php $s=0; ?>
		
		<?php $idProject = query("SELECT * FROM accepted_project WHERE id = $idp"); ?>
		<?php for ($i =0; $i <= count($idProject); $i++): ?>
		<div class="project-container">
			<div class="project-form s1" id="s1">
				<div class="project-body">
					<div class="project-step"><i class="fas fa-info"></i></div>
					<div class="project-step-2">Ongoing Project</div>
					<?php if ($pro < 1): ?>
						<div class="project-title">
							<?php echo "THERE IS NO ON GOING PROJECT"; ?></div>
						<div class="project-input">
					<?php else: ?>
							<div class="project-title">
								<?php echo $det["nama_project"]; ?></div>
							
							<div class="project-input">
								<div class="project-input-container">
									<div class="project-input-title">
										Kebutuhan Fungsional Project :
									</div>
								
								</div>
								<div class="project-input-container">
									<div class="project-input-desc">
										<?php echo $det["kebutuhan_fungsional"]; ?>
									</div>
								
								</div>
							</div>
							<div class="project-input">
								<div class="project-input-container">
									<div class="project-input-title">
										Platform Project :
									</div>
								
								</div>
								<div class="project-input-container">
									<div class="project-input-desc">
										<?php echo $det["platform"]; ?>
									</div>
								
								</div>
							</div>
							<div class="project-input">
								<div class="project-input-container">
									<div class="project-input-title">
										Jenis Project :
									</div>
								
								</div>
								<div class="project-input-container">
									<div class="project-input-desc">
										<?php echo $det["jenis_website"]; ?>
									</div>
								
								</div>
							</div>
							<div class="flex">
								<div class="project-submit-container">
									<div class="project-submit-back" onclick="Prev()">Kembali</div>
									<div onclick="Next()" class="project-submit" name="submit">Lihat Progress <span><i class="fas fa-chevron-right" style="margin-left: 5px;"></i></span></div>
								</div>

							</div>
						
				</div>
			</div>
			<div class="project-form s2" id="s2">
				<div class="project-body">
					<div class="project-step"><i class="fas fa-info"></i></div>
					<div class="project-step-2">Statistik Project</div>
					<div class="project-title"><?php echo $det["nama_project"]; ?></div>
					<div class="project-input-progress">
						<div class="project-det-container" style="margin-bottom: 1em">
							<div class="project-thumbnail-container" style="box-shadow: 0px 2px 6px #b9b9b9;">
								<img width="100%" height="100%" src="<?php echo $image_url ?>app.png">
							</div>
							<div class="project-progress-container">
								<div class="project-progress-title">Progress Project Anda</div>
								<div class="project-progress-bar-container">
									<div class="project-progress-bar">
										<div class="project-progress-bar-pointer" style="width:<?php echo $progress ?>%"></div>
									</div>
									<div class="project-progress-precentage"><?php echo $progress ?>%</div>
									<?php
										$t = $cp['tahap_project'];
										$tahap = array("Tahap diskusi","Tahap Pembentukan Tim","Tahap Pembuatan Feature","Tahap Pengujian","Tahap Penyelesaian","Selesai");
										for($i = 0; $i < count($tahap); $i++){
											if($t == $tahap[$i]){
												$indextahap = $i;
											}
										}
										$indextahap_lalu0 = array_search($tahap[0], $tahap);
										$indextahap_lalu1 = array_search($tahap[1], $tahap);
										$indextahap_lalu2 = array_search($tahap[2], $tahap);
										$indextahap_lalu3 = array_search($tahap[3], $tahap);
										$indextahap_lalu4 = array_search($tahap[4], $tahap);
										$indextahap_lalu5 = array_search($tahap[5], $tahap);


									 ?>
									<div class="project-progress-step-container">
										<?php if($t == $tahap[0]){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 1 : Tahap diskusi</div>
											</div>
										<?php }else if($indextahap_lalu0 <= $indextahap){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 1 : Tahap diskusi</div>
											</div>
										<?php }else{ ?>
											<div class="project-progress-step">
												<i class="fas fa-spinner-third project-progress-step-icon-ongoing"></i>
												<div class="project-progress-step-title-ongoing">Tahap 1 : Tahap diskusi</div>
											</div>
										<?php } ?>
										<?php if($t == $tahap[1]){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 2 : Tahap Pembentukan Tim</div>
											</div>
										<?php }else if($indextahap_lalu1 <= $indextahap){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 2 : Tahap Pembentukan Tim</div>
											</div>
										<?php }else{ ?>
											<div class="project-progress-step">
												<i class="fas fa-spinner-third project-progress-step-icon-ongoing"></i>
												<div class="project-progress-step-title-ongoing">Tahap 2 : Tahap Pembentukan Tim</div>
											</div>
										<?php } ?>

										<?php if($t == $tahap[2]){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 3 : Tahap Pembuatan Feature</div>
											</div>
										<?php }else if($indextahap_lalu2 <= $indextahap){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 3 : Tahap Pembuatan Feature</div>
											</div>
										<?php }else{ ?>
											<div class="project-progress-step">
												<i class="fas fa-spinner-third project-progress-step-icon-ongoing"></i>
												<div class="project-progress-step-title-ongoing">Tahap 3 : Tahap Pembuatan Feature</div>
											</div>
										<?php } ?>

										<?php if($t == $tahap[3]){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 4 : Tahap Pengujian</div>
											</div>
										<?php }else if($indextahap_lalu3 <= $indextahap){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 4 : Tahap Pengujian</div>
											</div>
										<?php }else{ ?>
											<div class="project-progress-step">
												<i class="fas fa-spinner-third project-progress-step-icon-ongoing"></i>
												<div class="project-progress-step-title-ongoing">Tahap 4 : Tahap Pengujian</div>
											</div>
										<?php } ?>

										<?php if($t == $tahap[4]){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 5 : Tahap Penyelesaian</div>
											</div>
										<?php }else if($indextahap_lalu4<= $indextahap){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 5 : Tahap Penyelesaian</div>
											</div>
										<?php }else{ ?>
											<div class="project-progress-step">
												<i class="fas fa-spinner-third project-progress-step-icon-ongoing"></i>
												<div class="project-progress-step-title-ongoing">Tahap 5 : Tahap Penyelesaian</div>
											</div>
										<?php } ?>
										<?php if($t == $tahap[5]){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 6 : Selesai</div>
											</div>
										<?php }else if($indextahap_lalu5<= $indextahap){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 6 : Selesai</div>
											</div>
										<?php }else{ ?>
											<div class="project-progress-step">
												<i class="fas fa-spinner-third project-progress-step-icon-ongoing"></i>
												<div class="project-progress-step-title-ongoing">Tahap 6 : Selesai</div>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
								
							
						
					<?php endif; ?>
						</div>
					</div>
					<div class="flex">
						<div class="project-submit-container">
							<div class="project-submit-back" onclick="Prev()">Kembali</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $i++; ?>
		<?php endfor; ?>
	</div>
</body>
<script type="text/javascript">
	function Next(){
			var s1 = document.getElementById('s1');
			var s2 = document.getElementById('s2');
			s1.style.display = "none";
			s2.style.display = "block";
		}
	function Prev(){
		var s1 = document.getElementById('s1');
		var s2 = document.getElementById('s2');
		s1.style.display = "block";
		s2.style.display = "none";

	}
</script>
</html>