<?php 
	session_start();
	include "functions.php";
	if (!isset($_SESSION["login"])) {
		header("Location: login.php");
		exit;
	}

	if (isset($_POST["kirim"]))
	{
		if (requestProject($_POST) > 0 )
		{

			echo "
				<script>
					alert('Terkirim!');

				</script>

			";
			header("Location: user_dashboard.php");
			exit;
		}
		else
		{
			echo "
				<script>
					alert('Gagal terkirim!');

				</script>
		";
		}
	}



 ?>
<!DOCTYPE html>
<?php 
	$image_url = "assets/images/";
	$css_url = "assets/css/";
 ?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Request Project</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #f1f1f1;
		}
	</style>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
				include 'includes/navbar.php';
				 ?>
			</div>
		</div>
	</nav>
	<style type="text/css">
		body{
			background-color: #2586d4;
		}
		::-webkit-input-placeholder { /* Chrome/Opera/Safari */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		::-moz-placeholder { /* Firefox 19+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-ms-input-placeholder { /* IE 10+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-moz-placeholder { /* Firefox 18- */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
	</style>
	<?php 
		if (isset($_POST['submit'])) {

			// Buat Nyari Value Platform yang dipilih
			$test = $_POST['pilih_platform'];
			foreach ($test AS $p) {
				if ($p != "") {
					$platform = $p; //ini valuenya Platform
				}
			}

			// Buat Nyari Value Kebutuhan Fungsional yang diinput (dijadikan Array);
			$kf = $_POST['fungsi'];
			$kebutuhan_fungsional = implode(",",$kf); //ini valuenya kebutuhan fungsional
		}
	 ?>
	<form method="post" onkeydown="return event.key != 'Enter';">
		<div class="request-content">
			<div class="request-container">
				<div class="request-form s1" id="s1">
					<div class="request-body">
						<div class="request-step"><i class="fas fa-plus"></i></div>
						<div class="request-step-2">Step 1: Project info</div>
						<div class="request-title">Request Project</div>
						<input type="hidden" name="id_client" value="<?php echo ($_SESSION["id_client"][0]["id"]); ?>">
						<div class="request-input-container">
							<div class="request-input-title">
								Judul Project
							</div>
							<input class="request-input" placeholder="Masukkan Judul Project" type="text" name="judul_project">
						</div>
						<div class="request-input-container">
							<div class="request-input-title">
								Deskripsi Project
							</div>
							<textarea class="request-textarea" placeholder="Masukkan Judul Project" type="text" name="deskripsi_project"></textarea> 
						</div>
						<div class="request-input-container">
							<div class="request-input-title">
								Platform Project
							</div>
							<div class="pure-grid">
								<div class="col-4 request-platform-active" id="web" onclick="pilih_web()">
									<div class="request-text" style="position: absolute;">
										Website
									</div>
									<input hidden type="text" id="pilih_web" value="Website" name="pilih_platform[]">
									<div class="request-selected-check" id="web_check" style="position: absolute;display: block">
										<i class="fas fa-check"></i>
									</div>
									<img class="request-thumbnail" width="100%" height="100%" src="<?php echo $image_url ?>web.jpg">
								</div>
								<div class="col-4 request-platform" id="mobile" onclick="pilih_app()">
									<div class="request-text" style="position: absolute;">
										Mobile
									</div>
									<input hidden type="text" id="pilih_app" value="" name="pilih_platform[]">
									<div class="request-selected-check" id="mobile_check" style="position: absolute;display: none">
										<i class="fas fa-check"></i>
									</div>
									<img class="request-thumbnail" width="100%" height="100%" src="<?php echo $image_url ?>app.png">
								</div>
							</div>
						</div>
						<div class="flex">
							<div class="request-submit-container">
								<div class="request-submit-back">Kembali</div>
								<div class="request-submit" onclick="Next()">Next <span><i class="fas fa-chevron-right" style="margin-left: 5px;"></i></span></div>	
							</div>
						</div>
					</div>
				</div>
				<div class="request-form s2" id="s2">
					<div class="request-body">
						<div class="request-step"><i class="fas fa-plus"></i></div>
						<div class="request-step-2">Step 2: Project Detail</div>
						<div class="request-title">Request Project</div>
						<div class="request-input-container">
							<div class="request-input-title">
								Jenis Project/Aplikasi
							</div>
							<select class="request-input" name="jenis_project">
								<option selected="true" disabled="disabled">-- Pilih Jenis Project --</option>
								<option value="E-Commerce">Aplikasi E-Commerce</option>
								<option value="Statistik">Aplikasi Statistik</option>
								<option value="Edukasi">Aplikasi Edukasi</option>
								<option value="Bisnis">Aplikasi Bisnis</option>
							</select>
						</div>
						<div class="request-input-container">
							<div class="request-input-title">
								Kebutuhan Fungsional Project
							</div>
							<input class="request-tag-input" placeholder="Masukkan Kebutuhan Fungsional" type="text" name="kebutuhanFungsional">
							<div class="request-tag-container">
								<div class="request-tag">
									<span>Login</span>
									<i class="fas fa-times"></i>
								</div>
								<div class="request-tag">
									<span>Buat Akun</span>
									<i class="fas fa-times"></i>
								</div>
							</div>
						</div>
						<div class="flex">
							<div class="request-submit-container">
								<div class="request-submit-back" onclick="Prev()">Kembali</div>
								<button class="request-submit" name="kirim">Kirim <span><i class="fas fa-chevron-right" style="margin-left: 5px;"></i></span></button>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<script type="text/javascript">
		const requesttagcontainer = document.querySelector('.request-tag-container');
		const input = document.querySelector('.request-tag-input');
		var tags = [];
		function buka() {
			var sidebar = document.querySelector('.sidebar-body');
        	if(sidebar.style.display == "block"){
        		sidebar.style.display = "none";
        	}else{
        		sidebar.style.display = "block";
        	}
		}
		function Next(){
			var s1 = document.getElementById('s1');
			var s2 = document.getElementById('s2');
			s1.style.display = "none";
			s2.style.display = "block";
		}
		function Prev(){
			var s1 = document.getElementById('s1');
			var s2 = document.getElementById('s2');
			s1.style.display = "block";
			s2.style.display = "none";

		}
		function pilih_web(){
			var c = document.getElementById('web');
			var cm = document.getElementById('mobile');
			var check_w = document.getElementById('web_check');
			var check_m = document.getElementById('mobile_check');
			var value_web = document.getElementById('pilih_web');
			var value_app = document.getElementById('pilih_app');
			var cc = c.getAttribute('class');
			if (cc == "col-4 request-platform") {
				c.setAttribute('class','col-4 request-platform-active');
				cm.setAttribute('class','col-4 request-platform');
				check_w.style.display = "block";
				check_m.style.display = "none";
				value_web.value = "Website";
				value_app.value = "";
			}else if(cc == "col-4 request-platform-active"){
				c.setAttribute('class','col-4 request-platform');
				check_w.style.display = "none";
				value_web.value = "";
				value_app.value = "";
			}

		}
		function pilih_app(){
			var c = document.getElementById('web');
			var cm = document.getElementById('mobile');
			var check_w = document.getElementById('web_check');
			var check_m = document.getElementById('mobile_check');
			var value_web = document.getElementById('pilih_web');
			var value_app = document.getElementById('pilih_app');
			var cc = cm.getAttribute('class');
			if (cc == "col-4 request-platform") {
				cm.setAttribute('class','col-4 request-platform-active');
				c.setAttribute('class','col-4 request-platform');
				check_w.style.display = "none";
				check_m.style.display = "block";
				value_app.value = "Mobile";
				value_web.value = "";
			}else if(cc == "col-4 request-platform-active"){
				cm.setAttribute('class','col-4 request-platform');
				check_m.style.display = "none";
				value_web.value = "";
				value_app.value = "";
			}

		}
		function submit() {
		  document.getElementById("form").submit();
		}
		function new_tag(label){
			const div = document.createElement('div');
			div.setAttribute('class','request-tag');
			const span = document.createElement('span');
			span.innerHTML = label;
			const close = document.createElement('i');
			const inp = document.createElement('input');
			inp.setAttribute('hidden','');
			inp.setAttribute('name','fungsi[]');
			inp.setAttribute('value',label);
			close.setAttribute('class','fas fa-times');
			close.setAttribute('data-item',label);
			div.appendChild(span);
			div.appendChild(inp);
			div.appendChild(close);
			return div;
		}
		function addTags() {
			resetTag();
			tags.forEach(function(tag){
				const input = new_tag(tag);
				requesttagcontainer.prepend(input);
			});
		}
		function resetTag() {
			 document.querySelectorAll('.request-tag').forEach(function (tag) {
			 	tag.parentElement.removeChild(tag);
			 })
		}
		input.addEventListener('keyup', function(e) {
			if (e.key == 'Enter') {
				tags.push(input.value);
				addTags();
				input.value = '';	
			}
		});
		document.addEventListener('click', function(e) {
			if (e.target.tagName == 'I') {
				const value = e.target.getAttribute('data-item');
				const index = tags.indexOf(value);
				tags = [...tags.slice(0,index),...tags.slice(index + 1)];
				addTags();
			}
		});
	</script>
</body> 
</html>