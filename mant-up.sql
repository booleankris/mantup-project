-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2020 at 05:17 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mant-up`
--

-- --------------------------------------------------------

--
-- Table structure for table `accepted_project`
--

CREATE TABLE `accepted_project` (
  `id` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `progress` varchar(255) NOT NULL,
  `estimasi` date DEFAULT NULL,
  `tahap_project` enum('Tahap diskusi','Tahap Pembentukan Tim','Tahap Pembuatan Feature','Tahap Pengujian','Tahap Penyelesaian','Selesai') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accepted_project`
--

INSERT INTO `accepted_project` (`id`, `id_project`, `tanggal_mulai`, `progress`, `estimasi`, `tahap_project`) VALUES
(1, 1, '2020-11-26', '25', '2020-11-30', 'Tahap diskusi'),
(2, 2, '2020-11-26', '50', '2020-11-30', 'Tahap Pembentukan Tim'),
(7, 8, '2020-11-11', '50', '2020-11-30', 'Tahap Pembuatan Feature');

-- --------------------------------------------------------

--
-- Table structure for table `assigned_feature`
--

CREATE TABLE `assigned_feature` (
  `id` int(11) NOT NULL,
  `id_contributor_project` int(11) NOT NULL,
  `id_roles` int(11) NOT NULL,
  `feature_name` varchar(255) NOT NULL,
  `status` enum('On Progress','Done') NOT NULL DEFAULT 'On Progress',
  `progress` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `assigned_feature`
--

INSERT INTO `assigned_feature` (`id`, `id_contributor_project`, `id_roles`, `feature_name`, `status`, `progress`) VALUES
(6, 11, 1, 'Login', 'On Progress', '0');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `nama_client` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `noHp` varchar(255) NOT NULL,
  `fotoProfil` varchar(255) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT 0,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `nama_client`, `email`, `password`, `noHp`, `fotoProfil`, `status`, `token`) VALUES
(3, 'irfan', 'irfansyapar007@gmail.com', '$2y$10$ACwE9Q.Wcjb.35w7Uza6EeyMbYFD1I3G0UtBJm3d.J0L31hba387q', '123', '', 0, ''),
(4, 'Jack Swagger', 'js@gmail.com', '$2y$10$5bI02gwArwTniJ/vRE9tUu5rnHlwYGGdJ3l9MZjuB13e3FpcwH28i', '123', '5fb69fb99071d.jpg', 0, ''),
(5, 'Gabriel', 'gabriel@gmail.com', '$2y$10$1vpHZNjLZ.KlWnDWYwZlg.7JyQXEvjPHOcVLbIjt./98nvnRW7A5.', '085349130854', '5fb9b5c19a6b6.jpeg', 0, ''),
(6, 'gebs', 'gabriel123@gmail.com', '$2y$10$ITD77W4c/erA8GVeWUtQFeZENmVvn7XrrEycHUyxjOLijD/c48vni', '07674654645', '5fc2b0bee367e.png', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `contributor`
--

CREATE TABLE `contributor` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `noHp` varchar(255) NOT NULL,
  `fotoProfil` varchar(255) NOT NULL,
  `status` int(2) NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contributor`
--

INSERT INTO `contributor` (`id`, `nama`, `email`, `password`, `noHp`, `fotoProfil`, `status`, `token`) VALUES
(5, 'gabs@gmail.com', 'gabs@gmail.com', '$2y$10$fyGfh.F4F1r.cTNPOCS6Nu7/EuSjOZUn/AXAH6nhLe.miR3x5vSGa', '084343534535', '5fc48f49170c8.png', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `contributor_project`
--

CREATE TABLE `contributor_project` (
  `id` int(11) NOT NULL,
  `id_accepted_project` int(11) NOT NULL,
  `id_contributor` int(11) NOT NULL,
  `id_permission` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contributor_project`
--

INSERT INTO `contributor_project` (`id`, `id_accepted_project`, `id_contributor`, `id_permission`) VALUES
(11, 2, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `nama_permission` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `nama_permission`) VALUES
(1, 'Developer'),
(2, 'Tester');

-- --------------------------------------------------------

--
-- Table structure for table `progress`
--

CREATE TABLE `progress` (
  `id` int(11) NOT NULL,
  `id_accepted_project` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `nama_project` varchar(255) NOT NULL,
  `jenis_website` varchar(255) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `kebutuhan_fungsional` varchar(255) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `id_client`, `nama_project`, `jenis_website`, `platform`, `kebutuhan_fungsional`, `status`) VALUES
(1, 5, 'Sistem', 'Pendidikan', 'Mobile', 'login,register', 1),
(2, 4, 'Sistem Ternak Lele', 'Bisnis', 'Mobile', 'login,register,lele', 1),
(3, 6, 'Management Uang Kas', 'Bisnis', 'Website', 'pengeluaran', 1),
(8, 6, 'Sistem Penambang Batu Bara', 'Statistik', 'Website', 'Statistik,Harga,lii', 0),
(9, 6, 'Aplikasi Scan QR Code', 'Bisnis', 'Mobile', 'Verifikasi via QR,Login & Register,Scan QR', 0);

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(11) NOT NULL,
  `id_feature` int(11) NOT NULL,
  `report_type` varchar(255) NOT NULL,
  `isi report` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `id_contributor_project` int(11) NOT NULL,
  `nama_roles` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `id_contributor_project`, `nama_roles`) VALUES
(1, 11, 'Front-End Developer');

-- --------------------------------------------------------

--
-- Table structure for table `testimoni`
--

CREATE TABLE `testimoni` (
  `id` int(11) NOT NULL,
  `id_accepted_project` int(11) NOT NULL,
  `testimoni` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `testing`
--

CREATE TABLE `testing` (
  `id` int(11) NOT NULL,
  `id_accepted_project` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accepted_project`
--
ALTER TABLE `accepted_project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `assigned_feature`
--
ALTER TABLE `assigned_feature`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_accepted_project` (`id_contributor_project`),
  ADD KEY `id_roles` (`id_roles`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contributor`
--
ALTER TABLE `contributor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contributor_project`
--
ALTER TABLE `contributor_project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_accepted_project` (`id_accepted_project`),
  ADD KEY `id_contributor` (`id_contributor`),
  ADD KEY `id_roles` (`id_permission`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `progress`
--
ALTER TABLE `progress`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_accepted_project` (`id_accepted_project`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_client` (`id_client`) USING BTREE;

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_feature` (`id_feature`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_contributor_project` (`id_contributor_project`);

--
-- Indexes for table `testimoni`
--
ALTER TABLE `testimoni`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testing`
--
ALTER TABLE `testing`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_accepted_project` (`id_accepted_project`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accepted_project`
--
ALTER TABLE `accepted_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `assigned_feature`
--
ALTER TABLE `assigned_feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `contributor`
--
ALTER TABLE `contributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contributor_project`
--
ALTER TABLE `contributor_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `progress`
--
ALTER TABLE `progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `testimoni`
--
ALTER TABLE `testimoni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `testing`
--
ALTER TABLE `testing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accepted_project`
--
ALTER TABLE `accepted_project`
  ADD CONSTRAINT `accepted_project_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `assigned_feature`
--
ALTER TABLE `assigned_feature`
  ADD CONSTRAINT `assigned_feature_ibfk_1` FOREIGN KEY (`id_roles`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `assigned_feature_ibfk_2` FOREIGN KEY (`id_contributor_project`) REFERENCES `contributor_project` (`id`);

--
-- Constraints for table `contributor_project`
--
ALTER TABLE `contributor_project`
  ADD CONSTRAINT `contributor_project_ibfk_4` FOREIGN KEY (`id_accepted_project`) REFERENCES `accepted_project` (`id`),
  ADD CONSTRAINT `contributor_project_ibfk_6` FOREIGN KEY (`id_contributor`) REFERENCES `contributor` (`id`),
  ADD CONSTRAINT `contributor_project_ibfk_7` FOREIGN KEY (`id_permission`) REFERENCES `permission` (`id`);

--
-- Constraints for table `progress`
--
ALTER TABLE `progress`
  ADD CONSTRAINT `progress_ibfk_1` FOREIGN KEY (`id_accepted_project`) REFERENCES `accepted_project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `client` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`id_contributor_project`) REFERENCES `contributor_project` (`id`);

--
-- Constraints for table `testing`
--
ALTER TABLE `testing`
  ADD CONSTRAINT `testing_ibfk_1` FOREIGN KEY (`id_accepted_project`) REFERENCES `accepted_project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
