<!DOCTYPE html>
<?php 
	$image_url = "assets/images/";
	$css_url = "assets/css/";
 ?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Landing Page</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
</head>
<body>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
					include 'includes/landing_page_navbar.php';
				 ?>
			</div>
		</div>
	</nav>
	<!-- hero section -->
	<div class="main">
		<section class="hero">
			<div class="hero-container">
				<div class="pure-grid">
					<div class="col-6">
						<img width="100%" height="100%" src="<?php echo $image_url ?>home.svg">
					</div>
					<div class="col-6">
						<div class="hero-caption-container">
							<div class="hero-caption">
								We are...
							</div>
							<div class="hero-second-caption">Your Website and Software Development Partner</div>
							<div class="hero-desc-caption tc">Kami Membuat Website dan Software yang dapat mempermudah anda dalam membangun startup.Dengan bantuan dari team profesional kami,anda bisa membuat website dan software seperti yang anda inginkan</div>
							<div class="flex">
								<a href="register.php">
									<div class="flex-item">
										<div class="hero-caption-button">Daftar Sekarang</div>
									</div>
								</a>
								<a href="login.php">
									<div class="flex-item">
										<div class="hero-second-button">Login</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="content">
			wsss
		</section>
	</div>
</body> 
</html>