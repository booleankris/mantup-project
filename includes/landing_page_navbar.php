
<style type="text/css">
nav{
    position: absolute;
    z-index: 4;
}
.sidebar-right{
    width: 48%;
}
.sidebar-container{
    width: 100%;
    z-index: 2;
    position: fixed;
    top: 0;
}
.sidebar-shape-container{
    width: 72px;
    border-radius: 2px 2px 0px 60px;
    height: 61px;
    background: #ffffff;
    transition: 1s;
    position: absolute;
    right: 20px;
    top: 0px;
    transform: scale(1);
    transition: 1s;
    z-index: 6;
}
.sidebar-shape-container:hover {
    animation: navbar-animation-hover 1s;
}

.sidebar-body{
    display: none;
    width: 300px;
    height: 100vh;
    border-radius: 0px;
    background: #1b2a36;
    position: absolute;
    overflow-y: scroll;
    overflow-x: hidden;
    right: 0px;
    top: 0px;
    z-index: 5;
}
.sidebar-body::-webkit-scrollbar{
    display: none;
}
.sidebar-contents{
    position: relative;
    top: 70px;
/*    left: 12px;*/
}
.sidebar-icon-container{
    padding: 1.5em;
    width: 75%;
    transition: 0.7s;
}
.sidebar-menu-container{
    height: 50px;
    width: 50px;
    border-radius: 50%;
    background-color: #00000042;
    display: flex;
    margin: 0.5em 1.6em;
    transition: 0.8s;
}
.sidebar-menu{
    display: flex;
    transition: 0.7s;
}
.sidebar-menu-container img{
    padding: 0.8em 0.6em;
    box-sizing: border-box;
    object-fit: contain;
}
.sidebar-caption{
    padding: 0.8em;
    width: 300px;
    color: #fff;
    font-family: 'Quicksand';
    font-weight: bold;
}
.sidebar-menu:hover{
    background-color: #ed2d42;
    transition: 0.7s;
    padding-left: 0.5em;
    transform: scale(1.2);
    transform:translateX(20%);
    border-radius: 35px;
}
.sidebar-menu:hover .sidebar-menu-container{
    position: relative;
    transition: 0.7s;
    left: -84px;
    background: #ed2d42;
}
@keyframes navbar-animation{
    0%{
        width: 72px;
        border-radius: 2px 2px 0px 60px;
        height: 61px;
    }
    100%{
        width: 300px;
        height: 100vh;
        border-radius: 0px;
    }
}
@keyframes navbar-animation-out{
    0%{
        width: 300px;
        height: 100vh;
        border-radius: 0px;
    }
    100%{
        width: 72px;
        border-radius: 2px 2px 0px 60px;
        height: 61px;
    }
}
@keyframes navbar-animation-hover{
    0%{
        transform: scale(1);
        box-shadow: 0 0 0 0 #3ca8ffa3;
    }
    70%{
        transform: scale(1.2);
        box-shadow: 0 0 0 10px rgba(0, 0, 0, 0);  
    }
    100%{
        transform: scale(1);
        box-shadow: 0 0 0 0 rgba(0, 0, 0, 0); 
    }
}
.sidebar-shape-1{
    width: 45px;
    height: 4px;
    border-radius: 20px;
    background: #2196F3;
    position: absolute;
    top: 14px;
    right: 10px;
}
.sidebar-shape-2{
    width: 32px;
    height: 4px;
    border-radius: 20px;
    background: #2196F3;
    position: absolute;
    top: 26px;
    right: 10px;
}
.sidebar-shape-3{
    width: 22px;
    height: 4px;
    border-radius: 20px;
    background: #2196F3;
    position: absolute;
    top: 39px;
    right: 10px;
}
</style>
<div class="sidebar-container">
    <div class="sidebar-right float-right">
        <div class="sidebar-body">
            <div class="sidebar-contents">
                <div class="sidebar-icon-container">
                    <img width="100%" height="100%" src="assets/images/manajour.png" class="sidebar-icon">
                </div>
                <div class="sidebar-item">
                    <div class="sidebar-menu">
                        <div class="sidebar-menu-container">
                            <img width="100%" height="100%" src="assets/images/home.png" class="sidebar-icon">
                            <div class="sidebar-caption-container">
                                <div class="sidebar-caption">
                                    Home
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-menu">
                        <div class="sidebar-menu-container">
                            <img width="100%" height="100%" src="assets/images/akun.png" class="sidebar-icon">
                            <div class="sidebar-caption-container">
                                <div class="sidebar-caption">
                                    Tentang Kami
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-menu">
                        <div class="sidebar-menu-container">
                            <img width="100%" height="100%" src="assets/images/doorprize.png" class="sidebar-icon">
                            <div class="sidebar-caption-container">
                                <div class="sidebar-caption">
                                    Fitur Kami
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-menu">
                        <div class="sidebar-menu-container">
                            <img width="100%" height="100%" src="assets/images/logout.png" class="sidebar-icon">
                            <div class="sidebar-caption-container">
                                <div class="sidebar-caption">
                                    Login
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sidebar-shape-container" onclick="navbar()">
            <div class="sidebar-shape-1"></div>
            <div class="sidebar-shape-2"></div>
            <div class="sidebar-shape-3"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function navbar() {
         var navbar = document.querySelector('.sidebar-body');
        navbar.style.animation = 'navbar-animation-out 1s';
        if(navbar.style.display == "block"){
            setTimeout(function() { navbar.style.animation = 'navbar-animation-out 0.1s';navbar.style.display = "none"; }, 1000);
        }else{
          navbar.style.animation = 'navbar-animation 1s';
          navbar.style.display = "block";
        }
    }
</script>