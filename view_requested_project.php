<?php 
	include "functions.php";
	$ReqProject = query("SELECT * FROM project");
 ?>
<!DOCTYPE html>
<?php 
	$image_url = "assets/images/";
	$css_url = "assets/css/";
 ?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Ongoing Project</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #f1f1f1;
		}
	</style>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
				include 'includes/header.php';

				 ?>
			</div>
		</div>
	</nav>
	<style type="text/css">
		body{
			background-color: #2586d4;
		}
		::-webkit-input-placeholder { /* Chrome/Opera/Safari */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		::-moz-placeholder { /* Firefox 19+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-ms-input-placeholder { /* IE 10+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-moz-placeholder { /* Firefox 18- */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
	</style>
	<div class="project-content">
		<div class="project-container">
			<div class="project-form s1" id="s1">
				<?php foreach ($variable as $key => $value): ?>
					
				<?php endforeach ?>
				<div class="project-body">
					<div class="project-step"><i class="fas fa-info"></i></div>
					<div class="project-step-2">Requested Project</div>
					<div class="project-title">Sistem Peminjaman Dummy</div>
					<div class="project-input">
						<div class="project-input-container">
							<div class="project-input-title">
								Deskripsi Project :
							</div>
							
						</div>
						<div class="project-input-container">
							<div class="project-input-desc">
								Sistem Peminjaman Dummy Sistem Peminjaman Dummy Sistem Peminjaman Dummy
							</div>
							
						</div>
					</div>
					<div class="project-input">
						<div class="project-input-container">
							<div class="project-input-title">
								Kebutuhan Fungsional Project :
							</div>
							
						</div>
						<div class="project-input-container">
							<div class="project-input-desc">
								Sistem Peminjaman Dummy Sistem Peminjaman Dummy Sistem Peminjaman Dummy
							</div>
							
						</div>
					</div>
					<div class="project-input">
						<div class="project-input-container">
							<div class="project-input-title">
								Platform Project :
							</div>
							
						</div>
						<div class="project-input-container">
							<div class="project-input-desc">
								Website
							</div>
							
						</div>
					</div>
					<div class="project-input">
						<div class="project-input-container">
							<div class="project-input-title">
								Jenis Project :
							</div>
							
						</div>
						<div class="project-input-container">
							<div class="project-input-desc">
								Aplikasi Peminjaman
							</div>
							
						</div>
					</div>
					<div class="flex">
						<div class="project-submit-container">
							<div class="project-submit-back">Kembali</div>
							<div class="project-submit" name="submit">Accept Project <span><i class="fas fa-chevron-right" style="margin-left: 5px;"></i></span></div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	function Next(){
			var s1 = document.getElementById('s1');
			var s2 = document.getElementById('s2');
			s1.style.display = "none";
			s2.style.display = "block";
		}
	function Prev(){
		var s1 = document.getElementById('s1');
		var s2 = document.getElementById('s2');
		s1.style.display = "block";
		s2.style.display = "none";

	}
</script>
</html>