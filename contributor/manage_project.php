<!DOCTYPE html>
<?php 
	$image_url = "../assets/images/";
	$css_url = "../assets/css/";
	include 'auth.php';
	include '../functions.php';
 ?>

<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Projects</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #f1f1f1;
		}
	</style>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
				include '../includes/navbar.php';
				 ?>
			</div>
		</div>
	</nav>
	<style type="text/css">
		body{
			background-color: #f5f5f5;
		}
		::-webkit-input-placeholder { /* Chrome/Opera/Safari */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		::-moz-placeholder { /* Firefox 19+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-ms-input-placeholder { /* IE 10+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-moz-placeholder { /* Firefox 18- */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
	</style>
	<?php 
	if (isset($_GET['idp'])) {
		$idp = $_GET['idp'];
		$query = mysqli_query($conn,"SELECT accepted_project.* , project.* FROM accepted_project INNER JOIN project ON project.id = accepted_project.id_project WHERE accepted_project.id='$idp'");
		$project = mysqli_fetch_array($query);
	}
	 ?>
	<div class="mg-project-content">
		<div class="mg-project-container">
			<div class="mg-project-form-1 s1" id="s1">
				<div class="mg-project-body-1">
					<div class="mg-project-step"><i class="fas fa-info"></i></div>
					<div class="mg-project-step-2">Manage mg-project</div>
					<div class="mg-project-title"><?php echo $project['nama_project']; ?></div>
					<div class="mg-project-input">
						<div class="mg-project-input-container">
							<div class="mg-project-input-title">
								Deskripsi:
							</div>
							
						</div>
						<div class="mg-project-input-container">
							<div class="mg-project-input-desc">
								Dummy Dum
							</div>
							
						</div>
					</div>
					<div class="mg-project-input">
						<div class="mg-project-input-container">
							<div class="mg-project-input-title">
								Kebutuhan Fungsional mg-project :
							</div>
							
						</div>
						<div class="mg-project-input-container">
							<div class="mg-project-input-desc">
								<?php echo $project['kebutuhan_fungsional']; ?>
							</div>
							
						</div>
					</div>
					<div class="mg-project-input">
						<div class="mg-project-input-container">
							<div class="mg-project-input-title">
								Platform :
							</div>
							
						</div>
						<div class="mg-project-input-container">
							<div class="mg-project-input-desc">
								<?php echo $project['platform']; ?>
							</div>
							
						</div>
					</div>
					<div class="mg-project-input">
						<div class="mg-project-input-container">
							<div class="mg-project-input-title">
								Jenis mg-project :
							</div>
							
						</div>
						<div class="mg-project-input-container">
							<div class="mg-project-input-desc">
								<?php echo $project['jenis_website']; ?>
							</div>
							
						</div>
					</div>
					<div class="flex">
						<div class="mg-project-submit-container">
							<div class="mg-project-submit-back" onclick="Prev()">Kembali</div>
							<div onclick="Next()" class="mg-project-submit" name="submit">Lihat Progress <span><i class="fas fa-chevron-right" style="margin-left: 5px;"></i></span></div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mg-project-content-2">
		<div class="mg-project-container-2">
			<div class="mg-project-form s2" id="s2">
				<div class="mg-subpanel">
					<div class="mg-project-body" style="width: 36%;box-shadow: 0px 1px 6px #cdcdcd;
    border-radius: 12px;">
						<div class="mg-project-inner">
							<div class="mg-project-title"><?php echo $project['nama_project']; ?></div>
							<div class="mg-project-input-progress">
								<div class="mg-project-det-container" style="margin-bottom: 1em;">
									<div class="mg-project-progress-container">
										<div class="mg-project-progress-title">Progress</div>
										<div class="mg-project-progress-bar-container">
											<div class="mg-project-progress-bar">
												<div class="mg-project-progress-bar-pointer" style="width: <?php echo $project['progress']; ?>%"></div>
											</div>
											<div class="mg-project-progress-precentage"><?php echo $project['progress']; ?>%</div>
											<div class="mg-project-progress-step-container">
												<?php 
												$t = $project['tahap_project'];
												$tahap = array("Tahap diskusi","Tahap Pembentukan Tim","Tahap Pembuatan Feature","Tahap Pengujian","Tahap Penyelesaian","Selesai");
												for($i = 0; $i < count($tahap); $i++){
													if($t == $tahap[$i]){
														$indextahap = $i;
													}
												}
												$indextahap_lalu0 = array_search($tahap[0], $tahap);
												$indextahap_lalu1 = array_search($tahap[1], $tahap);
												$indextahap_lalu2 = array_search($tahap[2], $tahap);
												$indextahap_lalu3 = array_search($tahap[3], $tahap);
												$indextahap_lalu4 = array_search($tahap[4], $tahap);
												$indextahap_lalu5 = array_search($tahap[5], $tahap);
												 ?>
												<?php if($t == $tahap[0]){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 1 : Tahap diskusi</div>
											</div>
										<?php }else if($indextahap_lalu0 <= $indextahap){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 1 : Tahap diskusi</div>
											</div>
										<?php }else{ ?>
											<div class="project-progress-step">
												<i class="fas fa-spinner-third project-progress-step-icon-ongoing"></i>
												<div class="project-progress-step-title-ongoing">Tahap 1 : Tahap diskusi</div>
											</div>
										<?php } ?>
										<?php if($t == $tahap[1]){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 2 : Tahap Pembentukan Tim</div>
											</div>
										<?php }else if($indextahap_lalu1 <= $indextahap){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 2 : Tahap Pembentukan Tim</div>
											</div>
										<?php }else{ ?>
											<div class="project-progress-step">
												<i class="fas fa-spinner-third project-progress-step-icon-ongoing"></i>
												<div class="project-progress-step-title-ongoing">Tahap 2 : Tahap Pembentukan Tim</div>
											</div>
										<?php } ?>

										<?php if($t == $tahap[2]){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 3 : Tahap Pembuatan Feature</div>
											</div>
										<?php }else if($indextahap_lalu2 <= $indextahap){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 3 : Tahap Pembuatan Feature</div>
											</div>
										<?php }else{ ?>
											<div class="project-progress-step">
												<i class="fas fa-spinner-third project-progress-step-icon-ongoing"></i>
												<div class="project-progress-step-title-ongoing">Tahap 3 : Tahap Pembuatan Feature</div>
											</div>
										<?php } ?>

										<?php if($t == $tahap[3]){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 4 : Tahap Pengujian</div>
											</div>
										<?php }else if($indextahap_lalu3 <= $indextahap){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 4 : Tahap Pengujian</div>
											</div>
										<?php }else{ ?>
											<div class="project-progress-step">
												<i class="fas fa-spinner-third project-progress-step-icon-ongoing"></i>
												<div class="project-progress-step-title-ongoing">Tahap 4 : Tahap Pengujian</div>
											</div>
										<?php } ?>

										<?php if($t == $tahap[4]){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 5 : Tahap Penyelesaian</div>
											</div>
										<?php }else if($indextahap_lalu4<= $indextahap){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 5 : Tahap Penyelesaian</div>
											</div>
										<?php }else{ ?>
											<div class="project-progress-step">
												<i class="fas fa-spinner-third project-progress-step-icon-ongoing"></i>
												<div class="project-progress-step-title-ongoing">Tahap 5 : Tahap Penyelesaian</div>
											</div>
										<?php } ?>
										<?php if($t == $tahap[5]){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 6 : Selesai</div>
											</div>
										<?php }else if($indextahap_lalu5<= $indextahap){ ?>
											<div class="project-progress-step">
												<i class="fas fa-check project-progress-step-icon-success"></i>
												<div class="project-progress-step-title-success">Tahap 6 : Selesai</div>
											</div>
										<?php }else{ ?>
											<div class="project-progress-step">
												<i class="fas fa-spinner-third project-progress-step-icon-ongoing"></i>
												<div class="project-progress-step-title-ongoing">Tahap 6 : Selesai</div>
											</div>
										<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="mg-project-body" style="width: 60%;margin: 1em 1em;border-radius: 12px;">
						<div class="mg-project-assigned-team">
							<div class="mg-project-title">Project Workplan</div>
							<div class="mg-project-input-progress">
								<div class="mg-project-det-container" style="margin-bottom: 1em">
									<div class="mg-project-progress-container">
									<a href="add_report.php">
										<div class="md-project-button-add-container" style="margin-right: .6em">
											<i class="fa fa-plus" style="position: absolute;padding: 0.4em 0.55em;top: 7px;background: #f6ce56;border-radius: 26px;color: #ffffff;"></i>
											<div style="margin-left: 2.5em;">Create Report</div>
										</div>
									</a>
										<div class="project-progress-title-onprogress"><i class="fas fa-warning mg-project-progress-step-icon-ongoing-brand"></i> You Still Have Some Unfinished Task!</div>
										<div class="mg-project-progress-bar-container">
											<div class="mg-project-contributor-container">
												<div class="mg-project-progress-step">
														<i class="fas fa-check mg-project-progress-step-icon-success"></i>
													<div class="mg-project-progress-step-title-success">3 Feature Done</div>
												</div>
												<div class="mg-project-progress-step">
													<i class="fas fa-spinner-third mg-project-progress-step-icon-ongoing"></i>
													<div class="mg-project-progress-step-title-ongoing">2 Feature On Progress
													</div>
												</div>
											</div>
										</div>
										<div class="">
											<table class="table-project-contributor" width="100%">
											    <thead>
											        <tr>
											        	
											            <th style="text-align: center;">Nama Feature</th>
											            <th style="text-align: center;">Status</th>
											            <th style="text-align: center;">Role</th>
									
											        </tr>
											    </thead>
											    <tbody>
											        <tr>
											        
											            <td>UI/UX Design</td>
											            <td><select class="mc-modul-status-cancelled">
											            	<option>Done</option>
											            	<option selected="">Terkendala</option>
											            	<option>On Progress</option>
											            	</select>
											            </td>
											            <td>
											            	Front-End Developer
											            </td>
				
											        </tr>
											        <tr>
											            <td>Transaction System</td>
											            <td><select class="mc-modul-status-done">
											            	<option>Done</option>
											            	<option>Terkendala</option>
											            	<option>On Progress</option>
											            </select></td>
											            <td>
											            	Front-End Developer
											            </td>
					
											        </tr>
											        <tr>
											            <td>Login System</td>
											            <td><select class="mc-modul-status-done">
											            	<option>Done</option>
											            	<option>Terkendala</option>
											            	<option>On Progress</option>
											            	</select>
											            </td>
											            <td>
											            	Front-End Developer
											            </td>
											          
											        </tr>
											        <tr>
											            <td>UI/UX Design</td>
											            <td><select class="mc-modul-status-onprogress">
											            	<option>Done</option>
											            	<option>Terkendala</option>
											            	<option selected>On Progress</option>
											            	</select>
											            </td>
											            <td>
											            	Back-End Developer
											            </td>
											            
											        </tr>
											    </tbody>
											</table>
											<div class="project-contributor-total">
												3 Done, 1 to go
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="mg-project-body" style="width: 100%;margin: 1em 1em;border-radius: 12px;">
						<div class="mg-project-assigned-team">
							<div class="mg-project-title">Project Contributor</div>
							<div class="mg-project-input-progress">
								<div class="mg-project-det-container" style="margin-bottom: 1em">
									<div class="mg-project-progress-container">
										<div class="mg-project-progress-title">Contributor dalam Project ini</div>
										<div class="mg-project-progress-bar-container">
											<div class="mg-project-contributor-container">
												<div class="mg-project-progress-step">
														<i class="fas fa-check mg-project-progress-step-icon-success"></i>
													<div class="mg-project-progress-step-title-success">Project Developer</div>
												</div>
												<div class="mg-project-progress-step">
														<i class="fas fa-check mg-project-progress-step-icon-success"></i>
													<div class="mg-project-progress-step-title-success">Project Tester</div>
												</div>
											</div>
										</div>
										<div class="">
											<table class="table-project-contributor" width="60%">
											    <thead>
											        <tr>
											        	<th style="width: 28px!important"></th>
											            <th>Nama</th>
											            <th>Permission</th>
											        </tr>
											    </thead>
											    <tbody>
											        <tr>
											            <td style="column-width: 100px">
												            <div class="project-contributor-avatar">
												            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
												           	</div>
											            </td>
											            <td>
											            	Gabriel Krisanthara
											        	</td>
											            <td>Developer</td>
											            
											        </tr>
											        <tr>
											        	<td style="column-width: 100px">
												            <div class="project-contributor-avatar">
												            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava.png">
												           	</div>
											            </td>
											            <td>Miftahul Haq</td>
											            <td>Tester</td>
											            
											        </tr>
											        <tr>
											        	<td style="column-width: 100px">
												            <div class="project-contributor-avatar">
												            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava3.png">
												           	</div>
											            </td>
											            <td>Irvan</td>
											            <td>Developer</td>
											            
											        </tr>
											    </tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	function Next(){
			var s1 = document.getElementById('s1');
			var s2 = document.getElementById('s2');
			s1.style.display = "none";
			s2.style.display = "block";
		}
	function Prev(){
		var s1 = document.getElementById('s1');
		var s2 = document.getElementById('s2');
		s1.style.display = "block";
		s2.style.display = "none";

	}
</script>
</html>