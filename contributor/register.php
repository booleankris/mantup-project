<?php
	include '../functions.php'; 
	$image_url = "../assets/images/";
	$css_url = "../assets/css/";
	if(isset($_POST["submit"])){
		
		if (registrasiContributor($_POST) > 0) {
			echo "<script>

			alert('Data telah terkirim');

			</script>";

			header("Location: index.php");
			exit;
		}else{
			echo mysqli_error($conn);
		}
	}

 ?>
<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Register | Contributor</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #2586d4;
		}
		::-webkit-input-placeholder { /* Chrome/Opera/Safari */
		  font-size: 12px;
		  font-family: 'Rubik';
		}
		::-moz-placeholder { /* Firefox 19+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		}
		:-ms-input-placeholder { /* IE 10+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		}
		:-moz-placeholder { /* Firefox 18- */
		  font-size: 12px;
		  font-family: 'Rubik';
		}
	</style>
	<div class="register-container">
		<div class="register-panel">
			<form action="" method="POST" enctype="multipart/form-data">
				<div class="register-title"> Contributor Register</div>
				<div class="login-icon-container">
					<img width="100%" height="100%" src="<?php echo $image_url ?>login_icon.png">
				</div>
				<div class="pure-grid">
					<div class="col-6 p-1">
						<div class="register-input-container">
							<div class="register-input-caption">Nama Anda</div>
							<input type="text" name="nama" class="register-input" placeholder="Masukkan Nama Anda" autocomplete="off">
						</div>
						<div class="register-input-container">
							<div class="register-input-caption">Email</div>
							<input type="email" name="email" class="register-input" placeholder="Masukkan Email Anda" autocomplete="off">
						</div>
						<div class="register-input-container">
							<div class="register-input-caption">Password</div>
							<input type="password" name="password" class="register-input" placeholder="Masukkan Password" autocomplete="off">
						</div>
					</div>
					<div class="col-6 p-1">
						<div class="register-input-container">
							<div class="register-input-caption">Nomor Telepon</div>
							<input type="number" name="noTlp" class="register-input" placeholder="Masukkan Nomor Anda" autocomplete="off">
						</div>
						<div class="register-input-container">
							<div class="register-input-caption">Foto Profil</div>
							<input type="file" name="FotoProfil" class="register-input" autocomplete="off">
						</div>
					
					</div>
				</div>
				<div class="register-submit">
				<input type="submit" name="submit" class="ri-submit">
			</div>				
			</div>
			</form>			
		</div>
	</div>
</body>
</html>