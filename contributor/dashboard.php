<!DOCTYPE html>
<html>
<?php 
include 'auth.php';
include '../functions.php';
 ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<?php 
		$image_url = "../assets/images/";
		$css_url = "../assets/css/";
	 ?>
	<title>Dashboard Contributor</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #f1f1f1;
		}
	</style>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
				include '../includes/navbar.php';
				 ?>
			</div>
		</div>
	</nav>
	<?php 
	$idc = $_SESSION['perms'];
	$get_permission = mysqli_query($conn,"SELECT * FROM contributor_project WHERE id_contributor ='$idc'");
	while($f_permission = mysqli_fetch_assoc($get_permission)){
		$ip = $f_permission['id_permission'];
		$source_permission = mysqli_query($conn,"SELECT * FROM permission WHERE id ='$ip'");
		$f_sp = mysqli_fetch_array($source_permission);
	 ?>
	
	<!-- Panel Section -->
	<?php if ($f_sp['nama_permission'] == "Developer"){ ?>
	<div class="main">
		<section class="hero">
			<div class="dashboard-container">
				<div class="pure-grid">
					<div class="col-6 dashboard-welcome-col">
						<img width="100%" height="100%" src="<?php echo $image_url ?>meeting.png">
					</div>
					<div class="col-6 dashboard-welcome-col">
						<div class="dashboard-caption-container">
							<div class="dashboard-second-caption">Welcome To Contributor Dashboard!</div>
							<div class="dashboard-desc-caption tc">Selamat Datang di Halaman Dashboard Anda!<br>Anda Telah Login Sebagai Developer.</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="content">
			<div class="content-left">
				<div class="helper-card">
					<div class="helper-card-inner">
						<a href="request.php">
						<div class="m-request-project-button">
							<i class="fa fa-sticky-note" style="position: absolute;padding: 0.7em 0.8em;top: 7px;background: #f6ce56;border-radius: 26px;color: #ffffff;"></i>
							<div style="margin-left: 3em;">Kerjakan Project</div>
						</div>
						</a>
					</div>
					<div class="content-menu-container">
						<a href="add_report.php">
							<div class="content-menu">
								<img class="content-menu-icon" height="100%" width="100%" src="<?php echo $image_url ?>progress.png"><div class="menu-title">Write A Report</div>
							</div>
						</a>
						<div class="content-menu">
							<i class="fas fa-sign-out-alt content-menu-icon"></i><span class="menu-title">Logout</span>
						</div>
					</div>
				</div>
			</div>
			<div class="content-right">
				<?php 
					$cari = mysqli_query($conn,"SELECT * FROM contributor_project WHERE id_contributor ='$idc'");
					$fc = mysqli_fetch_array($cari);
					$idd = $fc['id'];
					$idp = $fc['id_accepted_project'];
					$cari_feature = mysqli_query($conn,"SELECT accepted_project.* , project.* FROM accepted_project INNER JOIN project ON project.id = accepted_project.id_project WHERE accepted_project.id='$idp'");
					$hitung_feature = mysqli_query($conn,"SELECT COUNT(id) FROM contributor_project WHERE id='$idd'");
					$ft =mysqli_fetch_array($hitung_feature);
					$feature = mysqli_fetch_array($cari_feature);
				 ?>
				 <?php 
				 if ($ft < 0) {
				  ?>
				 <div>Pheww..No Feature Assigned To You Right Now!</div>
				<?php }else{ ?>
				<div class="p-ongoing-project-container">
					<div class="pd-ongoing-project">
						<div class="p-ongoing-project-inner">
							<div class="p-ongoing-title-container">
								<div class="p-ongoing-title">
									Project Assigned to You
								</div>
							</div>
							<img width="100%" height="100%" src="<?php echo $image_url ?>web.jpg">
						</div>
						<div class="p-ongoing-project-left">
							<div class="p-ongoing-project-name">
								<?php echo $feature['nama_project']; ?>
							</div>
							<a href="manage_project.php?idp=<?php echo $feature['id'] ?>">
								<div class="p-ongoing-project-go">
									Work on it!
								</div>
							</a>
						</div>
					</div>
				</div>
			<?php } ?>
			</div>
			
		</section>
	</div>
	<?php }else if($f_sp['nama_permission'] == "Tester"){ ?>
	<div class="main">
		<section class="hero">
			<div class="dashboard-container">
				<div class="pure-grid">
					<div class="col-6 dashboard-welcome-col">
						<img width="100%" height="100%" src="<?php echo $image_url ?>meeting.png">
					</div>
					<div class="col-6 dashboard-welcome-col">
						<div class="dashboard-caption-container">
							<div class="dashboard-second-caption">Welcome To Contributor Dashboard!</div>
							<div class="dashboard-desc-caption tc">Selamat Datang di Halaman Dashboard Anda!<br>Anda Telah Login Sebagai Tester.</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="content">
			<div class="content-left">
				<div class="helper-card">
					<div class="helper-card-inner">
						<a href="request.php">
						<div class="m-request-project-button">
							<i class="fa fa-sticky-note" style="position: absolute;padding: 0.7em 0.8em;top: 7px;background: #f6ce56;border-radius: 26px;color: #ffffff;"></i>
							<div style="margin-left: 3em;">Test A Project</div>
						</div>
						</a>
					</div>
					<div class="content-menu-container">
						<a href="all_project.php">
							<div class="content-menu">
								<img class="content-menu-icon" height="100%" width="100%" src="<?php echo $image_url ?>project.png"><div class="menu-title">Completed Projects</div>
							</div>
						</a>
						<a href="ongoing_project.php">
							<div class="content-menu">
								<img class="content-menu-icon" height="100%" width="100%" src="<?php echo $image_url ?>progress.png"><div class="menu-title">On-Going Project</div>
							</div>
						</a>
						<div class="content-menu">
							<i class="fas fa-sign-out-alt content-menu-icon"></i><span class="menu-title">Logout</span>
						</div>
					</div>
				</div>
			</div>
			<div class="content-right">
				<div class="p-ongoing-project-container">
					<div class="pd-ongoing-project">
						<div class="p-ongoing-project-inner">
							<div class="p-ongoing-title-container">
								<div class="p-ongoing-title">
									Project Assigned to You
								</div>
							</div>
							<img width="100%" height="100%" src="<?php echo $image_url ?>web.jpg">
						</div>
						<div class="p-ongoing-project-left">
							<div class="p-ongoing-project-name">
								Sistem Pendaftaran Online
							</div>
							<a href="user_ongoing_project.php">
								<div class="p-ongoing-project-go">
									Work on it!
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			
		</section>
	</div>
	<?php } } ?>
	<script type="text/javascript">
		function buka() {
			var sidebar = document.querySelector('.sidebar-body');
        	if(sidebar.style.display == "block"){
        		sidebar.style.display = "none";
        	}else{
        		sidebar.style.display = "block";
        	}
		}
	</script>
</body> 
</html>