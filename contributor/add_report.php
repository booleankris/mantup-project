<!DOCTYPE html>
<?php 
	$image_url = "../assets/images/";
	$css_url = "../assets/css/";
 ?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Request Project</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #f1f1f1;
		}
	</style>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
				include '../includes/navbar.php';
				 ?>
			</div>
		</div>
	</nav>
	<style type="text/css">
		body{
			background-color: #2586d4;
		}
		::-webkit-input-placeholder { /* Chrome/Opera/Safari */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		::-moz-placeholder { /* Firefox 19+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-ms-input-placeholder { /* IE 10+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-moz-placeholder { /* Firefox 18- */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
	</style>
	<?php 
		if (isset($_POST['submit'])) {
			$test = $_POST['test'];
			foreach($test as $value) {

   				echo($value[0]);

			}
		}
	 ?>
	<form method="post" onkeydown="return event.key != 'Enter';">
		<div class="request-content">
			<div class="request-container">
				<div class="request-form s1" id="s1">
					<div class="request-body">
						<div class="request-step"><i class="fas fa-plus"></i></div>
						<div class="request-step-2">Create A Report</div>
						<div class="request-title">Report</div>
						<div class="request-input-container">
							<div class="request-input-title">
								Jenis Report
							</div>
							<select class="request-input">
								<option>-- Jenis Report --</option>
								<option>Terkendala</option>
								<option>Tidak Terkendala</option>
							</select>
						</div>
						<div class="request-input-container">
							<div class="request-input-title">
								Isi Report	
							</div>
							<textarea class="request-textarea" placeholder="Masukkan Judul Project" type="text" name="deskripsi_project"></textarea> 
						</div>
						<div class="flex">
							<div class="request-submit-container">
								<div class="request-submit-back">Kembali</div>
								<div class="request-submit" onclick="Next()">Next <span><i class="fas fa-chevron-right" style="margin-left: 5px;"></i></span></div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<script type="text/javascript">
		function buka() {
			var sidebar = document.querySelector('.sidebar-body');
        	if(sidebar.style.display == "block"){
        		sidebar.style.display = "none";
        	}else{
        		sidebar.style.display = "block";
        	}
		}
		function Next(){
			var s1 = document.getElementById('s1');
			var s2 = document.getElementById('s2');
			s1.style.display = "none";
			s2.style.display = "block";
		}
		function Prev(){
			var s1 = document.getElementById('s1');
			var s2 = document.getElementById('s2');
			s1.style.display = "block";
			s2.style.display = "none";

		}
		function pilih_web(){
			

		}
		function pilih_app(){
			

		}
		function submit() {
		  document.getElementById("form").submit();
		}
		input.addEventListener('keyup', function(e) {
			if (e.key == 'Enter') {
				tags.push(input.value);
				addTags();
				input.value = '';	
			}
		});
	</script>
</body> 
</html>