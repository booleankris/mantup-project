<?php 
$conn= mysqli_connect("localhost", "root", "", "mant-up");

function query($query){
	global $conn;
	$result = mysqli_query($conn, $query);
	$rows = [];	
	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
		$rows[] = $row;
	}
	return $rows;
}

function addRole($data){
	$nama = $data["RoleName"];
	$query = "INSERT INTO roles VALUES ('', '$nama', '$email', '$password', '$noHp', '$foto', '', '')";
	mysqli_query($conn, $query);
	return mysqli_affected_rows($conn);
}

function registrasi($data){
	global $conn;

	$nama = stripslashes($data["nama"]);
	$email = stripcslashes(strtolower($data["email"]));
	$password = mysqli_real_escape_string($conn, $data["password"]);
	$noHp = stripslashes($data["noTlp"]);
	$foto = uploadFoto();


	$result = mysqli_query($conn, "SELECT email FROM client WHERE email = '$email' ");
	if (mysqli_fetch_assoc($result)){
		echo "<script>

		alert('Alamat Email Sudah terdaftar !');

		</script>";

		return false;
	}

	$password = password_hash($password, PASSWORD_DEFAULT);

	$query = "INSERT INTO client VALUES ('', '$nama', '$email', '$password', '$noHp', '$foto', '', '')";
	mysqli_query($conn, $query);
	return mysqli_affected_rows($conn);

}

function addContributor($data){
	global $conn;
	$id_acc_proj = stripslashes($data["id_acc_proj"]);
	$namaContributor = stripcslashes($data["contributor"]);
	$nama_permission = stripcslashes($data["permissionName"]);

	$getIDNamaContributor = query("SELECT * FROM contributor WHERE nama = '$namaContributor' ");
	$getNamaPermission = query("SELECT * FROM permission WHERE nama_permission = '$nama_permission'");

	$getIDNamaContributor2 = $getIDNamaContributor[0]["id"];
	$getIDNamaPermission = $getIDNamaContributor[0]["id"];
	$query = "INSERT INTO contributor_project VALUES('','$id_acc_proj', '$getIDNamaContributor2', '$getIDNamaPermission')";
	mysqli_query($conn, $query);
	return mysqli_affected_rows($conn);


}
function deleteContributorProject($data){
	global $conn;
	mysqli_query($conn, "DELETE FROM contributor_project WHERE id = $data");
	return mysqli_affected_rows($conn);
}
function registrasiContributor($data){
	global $conn;

	$nama = stripslashes($data["nama"]);
	$email = stripcslashes(strtolower($data["email"]));
	$password = mysqli_real_escape_string($conn, $data["password"]);
	$noHp = stripslashes($data["noTlp"]);
	$foto = uploadFoto();
	

	$result = mysqli_query($conn, "SELECT email FROM contributor WHERE email = '$email' ");
	if (mysqli_fetch_assoc($result)){
		echo "<script>

		alert('Alamat Email Sudah terdaftar !');

		</script>";

		return false;
	}

	$password = password_hash($password, PASSWORD_DEFAULT);

	$query = "INSERT INTO contributor VALUES ('', '$nama', '$email', '$password', '$noHp', '$foto', '', '')";
	mysqli_query($conn, $query);
	return mysqli_affected_rows($conn);

}
function uploadFoto(){
	$namaFile = $_FILES['FotoProfil']['name'];
	$ukuranFile = $_FILES['FotoProfil']['size'];
	$error = $_FILES['FotoProfil']['error'];
	$tmpName = $_FILES['FotoProfil']['tmp_name'];
	

	// cek apakah yang diup adalah gambar
	$ekstensiGambarValid = ['jpg','jpeg','png'];
	$eksetensiGambar = explode('.', $namaFile);
	$eksetensiGambar = strtolower(end($eksetensiGambar));
	if($error === 4){
		return NULL;	
	}
	if (!in_array($eksetensiGambar, $ekstensiGambarValid)) {
		echo "<script>
				alert('ekstensi tidak valid');
		   	</script>";
		return false;
	}
	$namaFileBaru = uniqid();
	$namaFileBaru .= ".";
	$namaFileBaru .= $eksetensiGambar;
	move_uploaded_file($tmpName, 'img/' . $namaFileBaru);
	return $namaFileBaru;
	
}
function requestProject($data){
	global $conn;
	$id_user = $data["id_client"];
	$judulProject = htmlspecialchars($data["judul_project"]);
	$jenisProject = htmlspecialchars($data["jenis_project"]);
	$kf = $_POST['fungsi'];
	$kebutuhan_fungsional = implode(",",$kf);
	$test = $_POST['pilih_platform'];
	foreach ($test AS $p) {
		if ($p != "") {
			$platform = $p; //ini valuenya Platform
		}
	}
	$query = "INSERT INTO project (id, id_client, nama_project, jenis_website, platform, kebutuhan_fungsional, status) VALUES ('', '$id_user', '$judulProject', '$jenisProject', '$platform','$kebutuhan_fungsional', 0)";
	mysqli_query($conn, $query);
	return mysqli_affected_rows($conn);
}
	


 ?>