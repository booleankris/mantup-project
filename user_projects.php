<!DOCTYPE html>
<?php 
	$image_url = "assets/images/";
	$css_url = "assets/css/";
	include 'functions.php';
 ?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Dashboard User</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #f1f1f1;
		}
	</style>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
				include 'includes/header.php';
				 ?>
			</div>
		</div>
	</nav>
	<!-- Panel Section -->
	<div class="main">
		<div class="mn-title-container">
			<div class="mn-title">My Projects</div>
			<a href="request.php">
				<div class="mn-add-button">
					<i class="fa fa-plus" style="position: absolute;padding: 0.7em 0.8em;top: 7px;background: #f6ce56;border-radius: 26px;color: #ffffff;"></i>
					<div style="margin-left: 3em;">Request Project</div>
				</div>
			</a>
			<div class="mn-inner-container">
				<div class="pure-grid">
					<?php 
							$cek_detail_project = mysqli_query($conn,"SELECT * FROM project");
							while($detail_project = mysqli_fetch_array($cek_detail_project)){
					 ?>
					<div class="col-4">
						<div class="mn-card" id="w" style="background-color: #f6b048;color:#fff;margin-bottom:1.3em;">
							<?php 
							if ($detail_project['id'] == 1){ ?>
							<a href="testimoni.php">
								<div class="manage-button-container" style="background-color: #2196f3;">
									<i class="manage-button-icon fas fa-list" style="background-color: #00BCD4;"></i>
									<span style="margin:0em 1em 0em 0.5em;font-family: 'Quicksand';font-weight:bold;">Ongoing</span>
								</div>
							</a>
							<?php }else{  ?>
								<a href="testimoni.php">
									<div class="manage-button-container" style="background-color: #fb6c62;">
										<i class="manage-button-icon fas fa-list" style="background-color: #f44336;"></i>
										<span style="margin:0em 1em 0em 0.5em;font-family: 'Quicksand';font-weight:bold;">Pending</span>
									</div>
								</a>
							<?php } ?>
							<div class="mn-platform"><?php echo $detail_project['platform']; ?></div>
							<div class="mn-card-header" id="i">
								<div class="mn-card-title" id="t"><?php echo $detail_project['nama_project']; ?></div>
							</div>
							<div class="mn-card-body">
								<div class="mn-card-desc" id="d">
									<?php echo $detail_project['jenis_website']; ?>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function buka() {
			var sidebar = document.querySelector('.sidebar-body');
        	if(sidebar.style.display == "block"){
        		sidebar.style.display = "none";
        	}else{
        		sidebar.style.display = "block";
        	}
		}
	</script>
</body> 
</html>