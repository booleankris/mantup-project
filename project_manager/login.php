<?php 
	session_start();
	include "../functions.php";
	
	if (isset($_POST["submit"])) {
		if($_POST["username"] === "manager" && $_POST["password"] === "manager"){
			header("Location: dashboard.php");
			$_SESSION["loginManager"] = true;
			exit;
		}else{

		}
	}
	

 ?>
<!DOCTYPE html>
<?php 
	$image_url = "../assets/images/";
	$css_url = "../assets/css/";
 ?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Login | Manager</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
</head>
<style type="text/css">
::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  font-size: 12px;
  font-family: 'Rubik';
}
::-moz-placeholder { /* Firefox 19+ */
  font-size: 12px;
  font-family: 'Rubik';
}
:-ms-input-placeholder { /* IE 10+ */
  font-size: 12px;
  font-family: 'Rubik';
}
:-moz-placeholder { /* Firefox 18- */
  font-size: 12px;
  font-family: 'Rubik';
}
</style>
<body>
	<div class="login-container">
		<div class="pure-grid no-gap">
			<div class="col-5 login-col">
				<div class="login-panel">
					<div class="login-title-container">
						<div class="login-title"> Manager Login</div>
						<div class="login-icon-container">
							<img width="100%" height="100%" src="<?php echo $image_url ?>login_icon.png">
						</div>
						<form action="" method="POST">
						<div class="login-input-container">
							<div class="li-caption">Username</div>
							<div class="username-icon-container">
								<i class="fas fa-user"></i>
							</div>
							<input type="text" name="username" class="li-username" placeholder="Masukkan Username">
							<div class="li-caption">Password</div>
							<div class="password-icon-container">
								<i class="fas fa-key"></i>
							</div>
							<input type="password" name="password" class="li-password" placeholder="Masukkan Password">
						</div>
						<div class="login-input-container">
							<input type="submit" name="submit" value="Login" class="li-submit">
						</div>
						</form>
						<div class="act">
							Belum Punya Akun? Klik <span class="act-go"><a href="registe">disini!</a></span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-7 login-col">
				<div class="login-right-panel">
					<img width="100%" height="100%" src="<?php echo $image_url ?>login_vector.png">
				</div>
			</div>
		</div>
	</div>
</body>
</html>
