<!DOCTYPE html>
<?php 
	$image_url = "../assets/images/";
	$css_url = "../assets/css/";
 ?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Add Role</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #f1f1f1;
		}
	</style>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
				include '../includes/navbar.php';
				 ?>
			</div>
		</div>
	</nav>
	<style type="text/css">
		body{
			background-color: #2586d4;
		}
		::-webkit-input-placeholder { /* Chrome/Opera/Safari */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		::-moz-placeholder { /* Firefox 19+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-ms-input-placeholder { /* IE 10+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-moz-placeholder { /* Firefox 18- */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
	</style>
	<?php 
		if (isset($_POST['submit'])) {
			$test = $_POST['contributor'];
			echo "<script>alert('".$test."')</script>";
		}
	 ?>
	<form method="post" onkeydown="return event.key != 'Enter';">
		<div class="request-content">
			<div class="request-container" style="width: 100%">
				<div class="request-form-nobefore s1" id="s1">
					<div class="request-body">
						<a href="add_feature.php">
							<div class="md-project-button-add-container" style="margin-right: .6em">
								<i class="fa fa-plus" style="position: absolute;padding: 0.4em 0.55em;top: 7px;background: #f6ce56;border-radius: 26px;color: #ffffff;"></i>
								<div style="margin-left: 2.5em;">Tambah Feature</div>
							</div>
						</a>
						<br>
						<table class="table-project-contributor" width="100%">
						    <thead>
						        <tr>
						        	
						            <th style="text-align: center;">Nama Feature</th>
						            <th style="width: 30px;text-align: center;"></th>
						            <th>Feature Handler</th>
						            <th style="text-align: center;">Status</th>
						            <th style="text-align: center;">Role</th>
						            <th style="text-align: center;">Progress</th>
						            <th style="text-align: center">Action</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						        
						            <td>UI/UX Design</td>
						            <td style="column-width: 100px">
							            <div class="project-contributor-avatar">
							            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
							           	</div>
						            </td>
						            <td>
						            	Gabriel Krisanthara
						        	</td>
						            <td><div class="mg-modul-status-onprogress">
						            	On Progress
						            </div></td>
						            <td>
						            	Front-End Developer
						            </td>
						            <td>
						            	<div class="md-project-progress-bar">
											<div class="md-project-progress-bar-pointer"></div>
										</div>
						            </td>
						            <td>
						            	<div style="display:flex;flex-wrap:wrap;justify-content: center;">
						            		<div class="crud-icon-container">
						            			<i class="fas fa-pen crud-icon-edit"></i>

						            		</div>
						            		<div class="crud-icon-container">
						            			<i class="fas fa-trash crud-icon-delete"></i>
						            			
						            		</div>
						            	</div>
						            </td>
						        </tr>
						        <tr>
						            <td>Transaction System</td>
						            <td style="column-width: 100px">
							            <div class="project-contributor-avatar">
							            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
							           	</div>
						            </td>
						            <td>
						            	Gabriel Krisanthara
						        	</td>
						            <td><div class="mg-modul-status-done">
						            	Done!
						            </div></td>
						            <td>
						            	Front-End Developer
						            </td>
						            <td>
						            	<div class="mg-project-progress-bar">
											<div class="mg-project-progress-bar-pointer"></div>
										</div>
						            </td>
						            <td>
						            	<div style="display:flex;flex-wrap:wrap;justify-content: center;">
						            		<div class="crud-icon-container">
						            			<i class="fas fa-pen crud-icon-edit"></i>

						            		</div>
						            		<div class="crud-icon-container">
						            			<i class="fas fa-trash crud-icon-delete"></i>
						            			
						            		</div>
						            	</div>
						            </td>
						        </tr>
						        <tr>
						            <td>Login System</td>
						            <td style="column-width: 100px">
							            <div class="project-contributor-avatar">
							            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
							           	</div>
						            </td>
						            <td>
						            	Gabriel Krisanthara
						        	</td>
						            <td><div class="mg-modul-status-done">
						            	Done!
						            </div></td>
						            <td>
						            	Front-End Developer
						            </td>
						            <td>
						            	<div class="mg-project-progress-bar">
											<div class="mg-project-progress-bar-pointer"></div>
										</div>
						            </td>
						            <td>
						            	<div style="display:flex;flex-wrap:wrap;justify-content: center;">
						            		<div class="crud-icon-container">
						            			<i class="fas fa-pen crud-icon-edit"></i>

						            		</div>
						            		<div class="crud-icon-container">
						            			<i class="fas fa-trash crud-icon-delete"></i>
						            			
						            		</div>
						            	</div>
						            </td>
						        </tr>
						        <tr>
						            <td>UI/UX Design</td>
						            <td style="column-width: 100px">
							            <div class="project-contributor-avatar">
							            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
							           	</div>
						            </td>
						            <td>
						            	Gabriel Krisanthara
						        	</td>
						            <td><div class="mg-modul-status-done">
						            	On Progress
						            </div></td>
						            <td>
						            	Back-End Developer
						            </td>
						            <td>
						            	<div class="mg-project-progress-bar">
											<div class="mg-project-progress-bar-pointer"></div>
										</div>
						            </td>
						            <td>
						            	<div style="display:flex;flex-wrap:wrap;justify-content: center;">
						            		<div class="crud-icon-container">
						            			<i class="fas fa-pen crud-icon-edit"></i>

						            		</div>
						            		<div class="crud-icon-container">
						            			<i class="fas fa-trash crud-icon-delete"></i>
						            			
						            		</div>
						            	</div>
						            </td>
						        </tr>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</form>
	<script type="text/javascript">
	

	</script>
</body> 
</html>