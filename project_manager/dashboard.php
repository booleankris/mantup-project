<?php 
	session_start();
	include "../functions.php";

	if(!($_SESSION["loginManager"])){
		header("Location: login.php");
		exit;
	}
	$tahap_project_selesai = "Selesai";
	$ProjectSelesai = query("SELECT COUNT(id) FROM accepted_project WHERE tahap_project = '$tahap_project_selesai'");
	$ProjectBelumSelesai = query("SELECT COUNT(id) FROM accepted_project WHERE tahap_project != '$tahap_project_selesai'");


 ?>
<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<?php 
		$image_url = "../assets/images/";
		$css_url = "../assets/css/";
	 ?>
	<title>Dashboard Manager</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #f1f1f1;
		}
	</style>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
				include '../includes/navbar.php';
				 ?>
			</div>
		</div>
	</nav>
	<!-- Panel Section -->
	<div class="main">
		<section class="hero">
			<div class="dashboard-container">
				<div class="pure-grid">
					<div class="col-6 dashboard-welcome-col">
						<img width="100%" height="100%" src="<?php echo $image_url ?>meeting.png">
					</div>
					<div class="col-6 dashboard-welcome-col">
						<div class="dashboard-caption-container">
							<div class="dashboard-second-caption">Welcome To Manager Dashboard!</div>
							<div class="dashboard-desc-caption tc">Selamat Datang di Halaman Dashboard Anda!<br>	Mulailah Membuat project anda lewat dashboard ini.</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="content">
			<div class="content-left">
				<div class="helper-card">
					<div class="helper-card-inner">
						<a href="requested_project.php">
						<div class="m-request-project-button">
							<i class="fa fa-sticky-note" style="position: absolute;padding: 0.7em 0.8em;top: 7px;background: #f6ce56;border-radius: 26px;color: #ffffff;"></i>
							<div style="margin-left: 3em;">Incoming Project</div>
						</div>
						</a>
					</div>
					<div class="content-menu-container">
						<a href="all_project.php">
							<div class="content-menu">
								<img class="content-menu-icon" height="100%" width="100%" src="<?php echo $image_url ?>project.png"><div class="menu-title">Completed Projects</div>
							</div>
						</a>
						<a href="ongoing_project.php">
							<div class="content-menu">
								<img class="content-menu-icon" height="100%" width="100%" src="<?php echo $image_url ?>progress.png"><div class="menu-title">On-Going Project</div>
							</div>
						</a>
						<div class="content-menu">
							<i class="fas fa-sign-out-alt content-menu-icon"></i><span class="menu-title">Logout</span>
						</div>
					</div>
				</div>
			</div>
			<div class="content-right">
				<div class="p-ongoing-project-container">
					<div class="p-ongoing-project">
						<div class="p-ongoing-project-inner">
							<div class="p-ongoing-title-container">
								<div class="p-ongoing-title">
									Latest Project
								</div>
							</div>
							<img width="100%" height="100%" src="<?php echo $image_url ?>web.jpg">
						</div>
						<div class="p-ongoing-project-left">
							<div class="p-ongoing-project-name">
								Sistem Pendaftaran Online
							</div>
							<a href="manage_project.php">
								<div class="p-ongoing-project-go">
									Manage Project
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="pure-grid">
					<div class="col-6">
						<div class="report-card">
							<div class="report-card-inner">
								<div class="card-icon-container">
									<img width="100%" height="100%" src="<?php echo $image_url ?>icon_finish.png">
								</div>
								<div class="report-card-inner">	
									<div class="card-caption">Project Selesai</div>
									<div class="card-value"><?php echo $ProjectSelesai[0]["COUNT(id)"]; ?></div>
								</div>
							</div>	
						</div>
					</div>
					<div class="col-6">
						<div class="report-card">
							<div class="report-card-inner">
								<div class="card-icon-container">
									<img width="100%" height="100%" src="<?php echo $image_url ?>icon_progress.png">
								</div>
								<div class="report-card-inner">	
									<div class="card-caption">Project Dalam Proses</div>
									<div class="card-value"><?php echo $ProjectBelumSelesai[0]["COUNT(id)"]; ?></div>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
			
		</section>
	</div>
	<script type="text/javascript">
		function buka() {
			var sidebar = document.querySelector('.sidebar-body');
        	if(sidebar.style.display == "block"){
        		sidebar.style.display = "none";
        	}else{
        		sidebar.style.display = "block";
        	}
		}
	</script>
</body> 
</html>