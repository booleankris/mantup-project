<!DOCTYPE html>
<?php 
	$image_url = "../assets/images/";
	$css_url = "../assets/css/";
 ?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Dashboard User</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #f1f1f1;
		}
	</style>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
				include '../includes/navbar.php';
				 ?>
			</div>
		</div>
	</nav>
	<!-- Panel Section -->
	<div class="main">
		<div class="mn-title-container">
			<div class="mn-title">Completed Projects</div>
			<div class="mn-inner-container">
				<div class="pure-grid">
					<div class="col-4">
						<div class="mn-card" id="w" style="background-color: #f6b048;color:#fff;">
							<a href="">
								<div class="delete-container">
									<i class="delete-icon fas fa-history"></i>
								</div>
							</a>
							<div class="mn-platform">Website</div>
							<div class="mn-card-header" id="i">
								<div class="mn-card-title" id="t">Project Dummy Dum</div>
							</div>
							<div class="mn-card-body">
								<div class="mn-card-desc" id="d">
									Project Dummy Dum
								</div>
							</div>
						</div>
					</div>
					<div class="col-4">
						<div class="mn-card" id="w" style="background-color: #f6b048;color:#fff;">
							<a href="">
								<div class="delete-container">
									<i class="delete-icon fas fa-history"></i>
								</div>
							</a>
							<div class="mn-platform">Website</div>
							<div class="mn-card-header" id="i">
								<div class="mn-card-title" id="t">Project Dummy Dum</div>
							</div>
							<div class="mn-card-body">
								<div class="mn-card-desc" id="d">
									Project Dummy Dum
								</div>
							</div>
						</div>
					</div>
					<div class="col-4">
						<div class="mn-card" id="w" style="background-color: #f6b048;color:#fff;">
							<a href="">
								<div class="delete-container">
									<i class="delete-icon fas fa-history"></i>
								</div>
							</a>
							<div class="mn-platform">Website</div>
							<div class="mn-card-header" id="i">
								<div class="mn-card-title" id="t">Project Dummy Dum</div>
							</div>
							<div class="mn-card-body">
								<div class="mn-card-desc" id="d">
									Project Dummy Dum
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function buka() {
			var sidebar = document.querySelector('.sidebar-body');
        	if(sidebar.style.display == "block"){
        		sidebar.style.display = "none";
        	}else{
        		sidebar.style.display = "block";
        	}
		}
	</script>
</body> 
</html>