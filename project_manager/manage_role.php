<!DOCTYPE html>
<?php 
	$image_url = "../assets/images/";
	$css_url = "../assets/css/";
 ?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Add Role</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #f1f1f1;
		}
	</style>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
				include '../includes/navbar.php';
				 ?>
			</div>
		</div>
	</nav>
	<style type="text/css">
		body{
			background-color: #2586d4;
		}
		::-webkit-input-placeholder { /* Chrome/Opera/Safari */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		::-moz-placeholder { /* Firefox 19+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-ms-input-placeholder { /* IE 10+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-moz-placeholder { /* Firefox 18- */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
	</style>
	<?php 
		if (isset($_POST['submit'])) {
			$test = $_POST['contributor'];
			echo "<script>alert('".$test."')</script>";
		}
	 ?>
	<form method="post" onkeydown="return event.key != 'Enter';">
		<div class="request-content">
			<div class="request-container">
				<div class="request-form-nobefore s1" id="s1">
					<div class="request-body">
						<a href="add_role.php">
							<div class="md-project-button-add-container" style="margin-right: .6em">
								<i class="fa fa-plus" style="position: absolute;padding: 0.4em 0.55em;top: 7px;background: #f6ce56;border-radius: 26px;color: #ffffff;"></i>
								<div style="margin-left: 2.5em;">Tambah Role</div>
							</div>
						</a><br>
						<table class="table-project-contributor">
						    <thead>
						        <tr>	
						            <th style="text-align: center;">Nama Role</th>
						            <th style="text-align: center;">Action</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td>
						            	Front-End Developer
						            </td>
						            <td>
						            	<div style="display:flex;flex-wrap:wrap;justify-content: center;">
						            		<div class="crud-icon-container">
						            			<i class="fas fa-pen crud-icon-edit"></i>

						            		</div>
						            		<div class="crud-icon-container">
						            			<i class="fas fa-trash crud-icon-delete"></i>
						            			
						            		</div>
						            	</div>
						            </td>
						        </tr>
						        <tr>
						            <td>
						            	Front-End Developer
						            </td>
						            <td>
						            	<div style="display:flex;flex-wrap:wrap;justify-content: center;">
						            		<div class="crud-icon-container">
						            			<i class="fas fa-pen crud-icon-edit"></i>

						            		</div>
						            		<div class="crud-icon-container">
						            			<i class="fas fa-trash crud-icon-delete"></i>
						            			
						            		</div>
						            	</div>
						            </td>
						        </tr>
						        <tr>
						            <td>
						            	Front-End Developer
						            </td>
						            <td>
						            	<div style="display:flex;flex-wrap:wrap;justify-content: center;">
						            		<div class="crud-icon-container">
						            			<i class="fas fa-pen crud-icon-edit"></i>

						            		</div>
						            		<div class="crud-icon-container">
						            			<i class="fas fa-trash crud-icon-delete"></i>
						            			
						            		</div>
						            	</div>
						            </td>
						        </tr>
						        <tr>
						            <td>
						            	Front-End Developer
						            </td>
						            <td>
						            	<div style="display:flex;flex-wrap:wrap;justify-content: center;">
						            		<div class="crud-icon-container">
						            			<i class="fas fa-pen crud-icon-edit"></i>

						            		</div>
						            		<div class="crud-icon-container">
						            			<i class="fas fa-trash crud-icon-delete"></i>
						            			
						            		</div>
						            	</div>
						            </td>
						        </tr>
						        <tr>
						            <td>
						            	Front-End Developer
						            </td>
						            <td>
						            	<div style="display:flex;flex-wrap:wrap;justify-content: center;">
						            		<div class="crud-icon-container">
						            			<i class="fas fa-pen crud-icon-edit"></i>

						            		</div>
						            		<div class="crud-icon-container">
						            			<i class="fas fa-trash crud-icon-delete"></i>
						            			
						            		</div>
						            	</div>
						            </td>
						        </tr>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</form>
	<script type="text/javascript">
	
		

document.addEventListener('click', function(e) {
	if (!e.target.closest('#myCustomSelect')) {
		// click outside of the custom group
		toggleList('Shut')
		setState('initial')
	} 
})

// FUNCTIONS 
// /////////////////////////////////

function toggleList(whichWay) {
	if (whichWay === 'Open') {
		csList.classList.remove('hidden-all')
		csSelector.setAttribute('aria-expanded', 'true')
	} else { // === 'Shut'
		csList.classList.add('hidden-all')
		csSelector.setAttribute('aria-expanded', 'false')
	}
}

function findFocus() {
	const focusPoint = document.activeElement
	return focusPoint
}

function moveFocus(fromHere, toThere) {
	// grab the currently showing options, which might have been filtered
	const aCurrentOptions = aOptions.filter(function(option) {
		if (option.style.display === '') {
			return true
		}
	})
	// don't move if all options have been filtered out
	if (aCurrentOptions.length === 0) {
		return
	}
	if (toThere === 'input') {
		csInput.focus()
	}
	// possible start points
	switch(fromHere) {
		case csInput:
			if (toThere === 'forward') {
				aCurrentOptions[0].focus()
			} else if (toThere === 'back') {
				aCurrentOptions[aCurrentOptions.length - 1].focus()
			}
			break
		case csOptions[0]: 
			if (toThere === 'forward') {
				aCurrentOptions[1].focus()
			} else if (toThere === 'back') {
				csInput.focus()
			}
			break
		case csOptions[csOptions.length - 1]:
			if (toThere === 'forward') {
				aCurrentOptions[0].focus()
			} else if (toThere === 'back') {
				aCurrentOptions[aCurrentOptions.length - 2].focus()
			}
			break
		default: // middle list or filtered items 
			const currentItem = findFocus()
			const whichOne = aCurrentOptions.indexOf(currentItem)
			if (toThere === 'forward') {
				const nextOne = aCurrentOptions[whichOne + 1]
				nextOne.focus()
			} else if (toThere === 'back' && whichOne > 0) {
				const previousOne = aCurrentOptions[whichOne - 1]
				previousOne.focus()
			} else { // if whichOne = 0
				csInput.focus()
			}
			break
	}
}

function doFilter() {
	const terms = csInput.value
	const aFilteredOptions = aOptions.filter(function(option) {
		if (option.innerText.toUpperCase().startsWith(terms.toUpperCase())) {
			return true
		}
	})
	csOptions.forEach(option => option.style.display = "none")
	aFilteredOptions.forEach(function(option) {
		option.style.display = ""
	})
	setState('filtered')
	updateStatus(aFilteredOptions.length)
}

function updateStatus(howMany) {
	csStatus.textContent = howMany + " options available."
}

function makeChoice(whichOption) {
	const optionTitle = whichOption.querySelector('strong')
	csInput.value = optionTitle.textContent
	moveFocus(document.activeElement, 'input')
	// update aria-selected, if using
}

function setState(newState) {
	switch (newState) {
		case 'initial': 
			csState = 'initial'
			break
		case 'opened': 
			csState = 'opened'
			break
		case 'filtered':
			csState = 'filtered'
			break
		case 'closed': 
			csState = 'closed'
	}
	// console.log({csState})
}

	function doKeyAction(whichKey) {
		const currentFocus = findFocus()
		switch(whichKey) {
			case 'Enter':
				if (csState === 'initial') { 
					// if state = initial, toggleOpen and set state to opened
					toggleList('Open')
					setState('opened')
				} else if (csState === 'opened' && currentFocus.tagName === 'LI') { 
					// if state = opened and focus on list, makeChoice and set state to closed
					makeChoice(currentFocus)
					toggleList('Shut')
					setState('closed')
				} else if (csState === 'opened' && currentFocus === csInput) {
					// if state = opened and focus on input, close it
					toggleList('Shut')
					setState('closed')
				} else if (csState === 'filtered' && currentFocus.tagName === 'LI') {
					// if state = filtered and focus on list, makeChoice and set state to closed
					makeChoice(currentFocus)
					toggleList('Shut')
					setState('closed')
				} else if (csState === 'filtered' && currentFocus === csInput) {
					// if state = filtered and focus on input, set state to opened
					toggleList('Open')
					setState('opened')
				} else { // i.e. csState is closed, or csState is opened/filtered but other focus point?
					// if state = closed, set state to filtered? i.e. open but keep existing input? 
					toggleList('Open')
					setState('filtered')
				}
				break

			case 'Escape':
				// if state = initial, do nothing
				// if state = opened or filtered, set state to initial
				// if state = closed, do nothing
				if (csState === 'opened' || csState === 'filtered') {
					toggleList('Shut')
					setState('initial')
				}
				break

			case 'ArrowDown':
				if (csState === 'initial' || csState === 'closed') {
					// if state = initial or closed, set state to opened and moveFocus to first
					toggleList('Open')
					moveFocus(csInput, 'forward')
					setState('opened')
				} else {
					// if state = opened and focus on input, moveFocus to first
					// if state = opened and focus on list, moveFocus to next/first
					// if state = filtered and focus on input, moveFocus to first
					// if state = filtered and focus on list, moveFocus to next/first
					toggleList('Open')
					moveFocus(currentFocus, 'forward')
				} 
				break
			case 'ArrowUp':
				if (csState === 'initial' || csState === 'closed') {
					// if state = initial, set state to opened and moveFocus to last
					// if state = closed, set state to opened and moveFocus to last
					toggleList('Open')
					moveFocus(csInput, 'back')
					setState('opened')
				} else {
					// if state = opened and focus on input, moveFocus to last
					// if state = opened and focus on list, moveFocus to prev/last
					// if state = filtered and focus on input, moveFocus to last
					// if state = filtered and focus on list, moveFocus to prev/last
					moveFocus(currentFocus, 'back')
				}
				break 
			default:
				if (csState === 'initial') {
					// if state = initial, toggle open, doFilter and set state to filtered
					toggleList('Open')
					doFilter()
					setState('filtered')
				} else if (csState === 'opened') {
					// if state = opened, doFilter and set state to filtered
					doFilter()
					setState('filtered')
				} else if (csState === 'closed') {
					// if state = closed, doFilter and set state to filtered
					doFilter()
					setState('filtered')
				} else { // already filtered
					doFilter()
				}
				break 
		}
	}
	</script>
</body> 
</html>