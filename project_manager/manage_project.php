<?php session_start();
	include '../functions.php';
	$id=$_GET["id"];
	if(!($_SESSION["loginManager"])){
		header("Location: login.php");
		exit;
	}
 ?>
<!DOCTYPE html>
<?php 
	$image_url = "../assets/images/";
	$css_url = "../assets/css/";
	$contributor = query("SELECT * FROM contributor_project INNER JOIN contributor on contributor_project.id_contributor = contributor.id INNER JOIN permission on contributor_project.id_permission = permission.id");
	
	$baru = query("SELECT * FROM '$contributor' WHERE id_accepted_project = '$id'");
	$idInAcceptedProject = query("SELECT * FROM accepted_project WHERE id = '$id'");
	$idProject = $idInAcceptedProject[0]["id_project"];
	$idInAcceptedProjectForContributor = $idInAcceptedProject[0]["id"];
	$idProjectForContributor = query("SELECT * FROM contributor_project WHERE id_accepted_project = '$idInAcceptedProjectForContributor'");
	$ProjectData = query("SELECT * FROM project WHERE id = '$idProject'");
	

 ?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Ongoing mg-project</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_url ?>style.css">
	<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<style type="text/css">
		body{
			background-color: #f1f1f1;
		}
	</style>
	<!-- navbar -->
	<nav>
		<div class="navbar-container">
			<div class="navbar-inner">
				<div class="navbar-brand-icon">
					<img width="100%" height="100%" src="<?php echo $image_url ?>manajour.png">
				</div>
				<?php 
				include '../includes/navbar.php';
				 ?>
			</div>
		</div>
	</nav>
	<style type="text/css">
		body{
			background-color: #f5f5f5;
		}
		::-webkit-input-placeholder { /* Chrome/Opera/Safari */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		::-moz-placeholder { /* Firefox 19+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-ms-input-placeholder { /* IE 10+ */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
		:-moz-placeholder { /* Firefox 18- */
		  font-size: 12px;
		  font-family: 'Rubik';
		  outline: none;
		}
	</style>
	<div class="mg-project-content">
		<div class="mg-project-container">
			<div class="mg-project-form-1 s1" id="s1">
				<div class="mg-project-body-1">
					<div class="mg-project-step"><i class="fas fa-info"></i></div>
					<div class="mg-project-step-2">Manage project</div>
					<div class="mg-project-title"><?php echo $ProjectData[0]["nama_project"] ?></div>
					<div class="mg-project-input">
						<div class="mg-project-input-container">
							<div class="mg-project-input-title">
								Deskripsi project :
							</div>
							
						</div>
						<div class="mg-project-input-container">
							<div class="mg-project-input-desc">
								dwdwdwd dummy dummy dum
							</div>
							
						</div>
					</div>
					<div class="mg-project-input">
						<div class="mg-project-input-container">
							<div class="mg-project-input-title">
								Kebutuhan Fungsional project :
							</div>
							
						</div>
						<div class="mg-project-input-container">
							<div class="mg-project-input-desc">
								<?php echo $ProjectData[0]["kebutuhan_fungsional"]; ?>
							</div>
							
						</div>
					</div>
					<div class="mg-project-input">
						<div class="mg-project-input-container">
							<div class="mg-project-input-title">
								Platform project :
							</div>
							
						</div>
						<div class="mg-project-input-container">
							<div class="mg-project-input-desc">
								<?php echo $ProjectData[0]["platform"]; ?>
							</div>
							
						</div>
					</div>
					<div class="mg-project-input">
						<div class="mg-project-input-container">
							<div class="mg-project-input-title">
								Jenis project :
							</div>
							
						</div>
						<div class="mg-project-input-container">
							<div class="mg-project-input-desc">
								<?php echo $ProjectData[0]["jenis_website"]; ?>
							</div>
							
						</div>
					</div>
					<div class="flex">
						<div class="mg-project-submit-container">
							<div class="mg-project-submit-back" onclick="Prev()">Kembali</div>
							<div onclick="Next()" class="mg-project-submit" name="submit">Lihat Progress <span><i class="fas fa-chevron-right" style="margin-left: 5px;"></i></span></div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mg-project-content-2">
		<div class="mg-project-container-2">
			<div class="mg-project-form s2" id="s2">
				<div class="mg-subpanel">
					<div class="mg-project-body" style="width: 36%;box-shadow: 0px 1px 6px #cdcdcd;
    border-radius: 12px;">	
						<div class="mg-project-inner">
							<div class="mg-project-title"><?php echo $ProjectData[0]["nama_project"]; ?></div>
							<?php   ?>
							<div class="mg-project-input-progress">
								<div class="mg-project-det-container" style="margin-bottom: 1em;">
									<div class="mg-project-progress-container">
										<div class="mg-project-progress-title">Progress mg-project</div>
										<div class="mg-project-progress-bar-container">
											<div class="mg-project-progress-bar">
												<div class="mg-project-progress-bar-pointer"></div>
											</div>
											<div class="mg-project-progress-precentage">50%</div>
											<div class="mg-project-progress-step-container">
												<div class="mg-project-progress-step">
														<i class="fas fa-check mg-project-progress-step-icon-success"></i>
													<div class="mg-project-progress-step-title-success">Tahap 1 : Diskusi/Planning</div>
												</div>
												<div class="mg-project-progress-step">
														<i class="fas fa-check mg-project-progress-step-icon-success"></i>
													<div class="mg-project-progress-step-title-success">Tahap 2 : Pembentukan Tim</div>
												</div>
												<div class="mg-project-progress-step">
														<i class="fas fa-spinner-third mg-project-progress-step-icon-ongoing"></i>
													<div class="mg-project-progress-step-title-ongoing">Tahap 3 : Pembuatan Feature</div>
												</div>
												<div class="mark-buttons">Mark As Done</div>
												<div class="mg-project-progress-step">
														<i class="fas fa-spinner-third mg-project-progress-step-icon-ongoing"></i>
													<div class="mg-project-progress-step-title-ongoing">Tahap 4 : Pengujian</div>
												</div>
												<div class="mark-buttons">Mark As Done</div>
												<div class="mg-project-progress-step">
														<i class="fas fa-spinner-third mg-project-progress-step-icon-ongoing"></i>
													<div class="mg-project-progress-step-title-ongoing">Tahap 5 : Penyelesaian</div>
												</div>
												<div class="mark-buttons">Mark As Done</div>
												<div class="mg-project-progress-step">
														<i class="fas fa-spinner-third mg-project-progress-step-icon-ongoing"></i>
													<div class="mg-project-progress-step-title-ongoing">Tahap 6 : Selesai</div>
												</div>
												<div class="mark-buttons">Mark As Done</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="mg-project-body" style="width: 60%;margin: 1em 1em;border-radius: 12px;">

						<div class="mg-project-assigned-team">
							<div class="mg-project-title">Project Contributor</div>
							<div class="mg-project-input-progress">
								<div class="mg-project-det-container" style="margin-bottom: 1em">
									<div class="mg-project-progress-container">
										<div class="mg-project-button-add-container">
											<i class="fa fa-plus" style="position: absolute;padding: 0.4em 0.55em;top: 7px;background: #f6ce56;border-radius: 26px;color: #ffffff;"></i>
											<a href="add_contributor.php?id=<?php echo $ProjectData[0]['id'] ?>"><div style="margin-left: 2.5em;">Tambah Contributor</div></a>
										</div>
										<div class="mg-project-progress-title">Contributor Lengkap!</div>
										<div class="mg-project-progress-bar-container">
											<div class="mg-project-contributor-container">
												<div class="mg-project-progress-step">
														<i class="fas fa-check mg-project-progress-step-icon-success"></i>
													<div class="mg-project-progress-step-title-success">Project Developer</div>
												</div>
												<div class="mg-project-progress-step">
														<i class="fas fa-check mg-project-progress-step-icon-success"></i>
													<div class="mg-project-progress-step-title-success">Project Tester</div>
												</div>
											</div>
										</div>
										<div class="">
											<table class="table-project-contributor" width="100%">
											    <thead>
											        <tr>
											        	<th></th>
											            <th>Nama</th>
											            <th>Permission</th>
											            <th>Action</th>
											        </tr>
											    </thead>
											    <?php $i=0; ?>
											    <?php foreach ($contributor as $data): ?>
											    <tbody>
											    	<?php $contributorID = $data["id_contributor"]; ?>
											    	<?php $contributorName = query("SELECT nama FROM contributor WHERE id = $contributorID"); ?>
											    	<?php $permissionID = $data["id_permission"]; ?>
											    	<?php $permissionName = query("SELECT nama_permission FROM permission WHERE id= $permissionID"); ?>


											        <tr>
											            <td style="column-width: 100px">
												            <div class="project-contributor-avatar">
												            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
												           	</div>
											            </td>
											            <td>
											            	<?php echo $data["nama"]; ?>
											            	<?php var_dump($contributor); ?>
											        	</td>

											            <td><?php echo $data["nama_permission"]; ?></td>
											            <td style="text-align: center;"><span style="padding: 0.4em;font-family: 'Quicksand';color: #f44336;font-weight: 600"><b><a href="hapus.php?id=<?php echo $data['id']; ?>">Remove</a></b></span><i class="project-contributor-action fas fa-minus"></i></td>
											        </tr>
												    
											        
											    </tbody>
											    
											    <?php $i+=1; ?>
											    <?php endforeach; ?>
											</table>
											<div class="project-contributor-total">
												2 Developer, 1 Tester
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="mg-project-body" style="width: 100%;margin: 1em 1em;border-radius: 12px;">
						<div class="mg-project-assigned-team">
							<div class="mg-project-title">Project Feature</div>
							<div class="mg-project-input-progress">
								<div class="mg-project-det-container" style="margin-bottom: 1em">
									<div class="mg-project-progress-container">
										<div class="flex">
											<a href="manage_feature.php">
												<div class="md-project-button-add-container" style="margin-right: .6em">
													<i class="fa fa-plus" style="position: absolute;padding: 0.4em 0.55em;top: 7px;background: #f6ce56;border-radius: 26px;color: #ffffff;"></i>
													<div style="margin-left: 2.5em;">Manage Feature</div>
												</div>
											</a>
											<a href="manage_role.php">
												<div class="md-project-button-add-container" style="margin-right: .6em">
													<i class="fa fa-plus" style="position: absolute;padding: 0.4em 0.55em;top: 7px;background: #f6ce56;border-radius: 26px;color: #ffffff;"></i>
													<div style="margin-left: 2.5em;">Manage Role</div>
												</div>
											</a>
										</div>
										<div class="project-progress-title-onprogress"><i class="fas fa-warning mg-project-progress-step-icon-ongoing-brand"></i> Feature Still Incomplete</div>

										<div class="mg-project-progress-bar-container">
											<div class="mg-project-contributor-container">
												<div class="mg-project-progress-step">
														<i class="fas fa-check mg-project-progress-step-icon-success"></i>
													<div class="mg-project-progress-step-title-success">3 Feature Done</div>
												</div>
												<div class="mg-project-progress-step">
													<i class="fas fa-spinner-third mg-project-progress-step-icon-ongoing"></i>
													<div class="mg-project-progress-step-title-ongoing">2 Feature On Progress
													</div>
												</div>
											</div>
										</div>
										<div class="">
											<table class="table-project-contributor" width="100%">
											    <thead>
											        <tr>
											        	
											            <th style="text-align: center;">Nama Feature</th>
											            <th style="width: 30px;text-align: center;"></th>
											            <th>Feature Handler</th>
											            <th style="text-align: center;">Status</th>
											            <th style="text-align: center;">Role</th>
											            <th style="text-align: center;">Progress</th>
											        </tr>
											    </thead>
											    <tbody>
											        <tr>
											        
											            <td>UI/UX Design</td>
											            <td style="column-width: 100px">
												            <div class="project-contributor-avatar">
												            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
												           	</div>
											            </td>
											            <td>
											            	Gabriel Krisanthara
											        	</td>
											            <td><div class="mg-modul-status-onprogress">
											            	On Progress
											            </div></td>
											            <td>
											            	Front-End Developer
											            </td>
											            <td>
											            	<div class="md-project-progress-bar">
																<div class="md-project-progress-bar-pointer"></div>
															</div>
											            </td>
											        </tr>
											        <tr>
											            <td>Transaction System</td>
											            <td style="column-width: 100px">
												            <div class="project-contributor-avatar">
												            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
												           	</div>
											            </td>
											            <td>
											            	Gabriel Krisanthara
											        	</td>
											            <td><div class="mg-modul-status-done">
											            	Done!
											            </div></td>
											            <td>
											            	Front-End Developer
											            </td>
											            <td>
											            	<div class="mg-project-progress-bar">
																<div class="mg-project-progress-bar-pointer"></div>
															</div>
											            </td>
											        </tr>
											        <tr>
											            <td>Login System</td>
											            <td style="column-width: 100px">
												            <div class="project-contributor-avatar">
												            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
												           	</div>
											            </td>
											            <td>
											            	Gabriel Krisanthara
											        	</td>
											            <td><div class="mg-modul-status-done">
											            	Done!
											            </div></td>
											            <td>
											            	Front-End Developer
											            </td>
											            <td>
											            	<div class="mg-project-progress-bar">
																<div class="mg-project-progress-bar-pointer"></div>
															</div>
											            </td>
											        </tr>
											        <tr>
											            <td>UI/UX Design</td>
											            <td style="column-width: 100px">
												            <div class="project-contributor-avatar">
												            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
												           	</div>
											            </td>
											            <td>
											            	Gabriel Krisanthara
											        	</td>
											            <td><div class="mg-modul-status-done">
											            	On Progress
											            </div></td>
											            <td>
											            	Back-End Developer
											            </td>
											            <td>
											            	<div class="mg-project-progress-bar">
																<div class="mg-project-progress-bar-pointer"></div>
															</div>
											            </td>
											        </tr>
											    </tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="mg-project-body" style="width: 100%;margin: 1em 1em;border-radius: 12px;">
						<div class="mg-project-assigned-team">
							<div class="mg-project-title">Project Report</div>
							<div class="mg-project-input-progress">
								<div class="mg-project-det-container" style="margin-bottom: 1em">
									<div class="mg-project-progress-container">
										<div class="">
											<table class="table-project-contributor" width="100%">
											    <thead>
											        <tr>
											        	
											            <th style="text-align: center;">Feature Dikerjakan</th>
											            <th style="width: 30px;text-align: center;"></th>
											            <th>Feature Handler</th>
											            <th style="text-align: center;">Report Status</th>
											            <th style="text-align: center;">Report</th>
											        </tr>
											    </thead>
											    <tbody>
											        <tr>
											        
											            <td>UI/UX Design</td>
											            <td style="column-width: 100px">
												            <div class="project-contributor-avatar">
												            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
												           	</div>
											            </td>
											            <td>
											            	Gabriel Krisanthara
											        	</td>
											        	<td><div class="mg-modul-status-done">
											            	Tidak Terkendala
											            </div></td>
											            <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
											            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
											            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
											            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
											          
											        </tr>
											        <tr>
											            <td>Transaction System</td>
											            <td style="column-width: 100px">
												            <div class="project-contributor-avatar">
												            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
												           	</div>
											            </td>
											            <td>
											            	Gabriel Krisanthara
											        	</td>
											        	<td><div class="mg-modul-status-onprogress">
											            	Terkendala
											            </div></td>
											            <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
											            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
											            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
											            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
											         
											        </tr>
											        <tr>
											            <td>Login System</td>
											            <td style="column-width: 100px">
												            <div class="project-contributor-avatar">
												            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
												           	</div>
											            </td>
											            <td>
											            	Gabriel Krisanthara
											        	</td>
											        	<td><div class="mg-modul-status-done">
											            	Tidak Terkendala
											            </div></td>
											            <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
											            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
											            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
											            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
											          
											        </tr>
											        <tr>
											            <td>Merging Gitlab Branches</td>
											            <td style="column-width: 100px">
												            <div class="project-contributor-avatar">
												            	<img height="100%" width="100%" src="<?php echo $image_url ?>ava2.png">
												           	</div>
											            </td>
											            <td>
											            	Gabriel Krisanthara
											        	</td>
											        	<td><div class="mg-modul-status-done">
											            	Tidak Terkendala
											            </div></td>
											            <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
											            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
											            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
											            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
											        </tr>
											    </tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	function Next(){
			var s1 = document.getElementById('s1');
			var s2 = document.getElementById('s2');
			s1.style.display = "none";
			s2.style.display = "block";
		}
	function Prev(){
		var s1 = document.getElementById('s1');
		var s2 = document.getElementById('s2');
		s1.style.display = "block";
		s2.style.display = "none";

	}
</script>
</html>